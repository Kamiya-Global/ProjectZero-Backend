// noinspection JSUnresolvedVariable,DuplicatedCode

require('dotenv').config();

const express = require('express');
const AdmZip = require('adm-zip');
const { v4: uuid } = require('uuid');
const uploadImage = require('../modules/imageUploader');
const kamiyaToolKit = require('../modules/toolKit');
const eventLogger = require('../modules/eventLogger');
const request = require('then-request');
const QRCode = require('qrcode');
const newBillingHistory = require('../modules/billingHistory');
const axios = require('axios');

const { PrismaClient } = require('@prisma/client');
const fs = require("fs");
const prisma = new PrismaClient();

const app = express.Router();

app.use(express.json({ limit:'100mb'}));

//pixInk Api

const setArtworkCheck = (ArtworkHashId, ArtworkMetaId, Generate) => {
    const Interval = setInterval(async () => {
        const ArtworkStatus = await checkArtworkStatus(ArtworkMetaId);
        const { status } = ArtworkStatus;
        if(status === 'generated') {
            clearInterval(Interval);
            Generate.metadata.jpg = ArtworkStatus.meta.generateResultUrl.jpg;
            await prisma.Generate.update({
                where: {
                    hashid: ArtworkHashId
                },
                data: {
                    status: 'generated',
                    metadata: JSON.stringify(Generate.metadata),
                    updatedAt: Date.now()
                }
            });
            const { pointsUsed } = ArtworkStatus;
            const User = await prisma.user.findUnique({
                where: {
                    id: Generate.userId
                }
            });
            if(User.NCCredit > 0) {
                await prisma.user.update({
                    where: {
                        id: Generate.userId
                    },
                    data: {
                        NCCredit: {
                            decrement: pointsUsed * 100
                        }
                    }
                });
                await newBillingHistory(Generate.userId, pointsUsed, 'imageGenerate-PixInk', 'NC');
                return;
            }
            await prisma.user.update({
                where: {
                    id: Generate.userId
                },
                data: {
                    credit: {
                        decrement: pointsUsed * 100
                    }
                }
            });
            await newBillingHistory(Generate.userId, pointsUsed, 'imageGenerate-PixInk');
        } else if(status === 'queue_failed') {
            await prisma.Generate.update({
                where: {
                    hashid: ArtworkHashId
                },
                data: {
                    status: 'queue_failed',
                    updatedAt: Date.now()
                }
            });
            clearInterval(Interval);
        } else if(status === 'risky') {
            await prisma.Generate.update({
                where: {
                    hashid: ArtworkHashId
                },
                data: {
                    status: 'risky',
                    updatedAt: Date.now()
                }
            });
            clearInterval(Interval);
        } else {
            await prisma.Generate.update({
                where: {
                    hashid: ArtworkHashId
                },
                data: {
                    status: status,
                    updatedAt: Date.now()
                }
            });
        }
    }, 2000);
}

(async () => {
    const created = await prisma.Generate.findMany({
        where: {
            status: 'created'
        }
    });
    for(let i in created) {
        created[i].metadata = JSON.parse(created[i].metadata);
        setArtworkCheck(created[i].hashid, created[i].metaid, created[i]);
    }
})();

app.get('/api/image/generate', async (req, res) => {
    const { id } = req.auth;
    const { start, take } = req.query;
    const Generate = await prisma.Generate.findMany({
        where: {
            userId: id
        },
        orderBy: {
            id: 'desc'
        },
        skip: Number(start) - 1,
        take: Number(take)
    });
    for(let i in Generate) {
        Generate[i].id = kamiyaToolKit.numberToHash(Generate[i].id);
        Generate[i].createdAt = kamiyaToolKit.formatTime(new Date(Number(Generate[i].createdAt)));
        Generate[i].updatedAt = kamiyaToolKit.formatTime(new Date(Number(Generate[i].updatedAt)));
        Generate[i].metadata = JSON.parse(Generate[i].metadata);
    }
    res.send({
        status: 200,
        message: 'OK',
        data: Generate
    });
});

app.get('/api/image/generate/:hashid', async (req, res) => {
    const { id } = req.auth;
    const { hashid } = req.params;
    const Generate = await prisma.Generate.findUnique({
        where: {
            hashid: hashid
        }
    });
    if(!Generate) {
        res.send({
            status: 403,
            message: 'Forbidden'
        });
        return;
    }
    if(Generate.userId !== id) {
        res.send({
            status: 403,
            message: 'Forbidden'
        });
        return;
    }
    Generate.id = kamiyaToolKit.numberToHash(Generate.id);
    Generate.createdAt = kamiyaToolKit.formatTime(new Date(Number(Generate.createdAt)));
    Generate.updatedAt = kamiyaToolKit.formatTime(new Date(Number(Generate.updatedAt)));
    Generate.metadata = JSON.parse(Generate.metadata);
    res.send({
        status: 200,
        message: 'OK',
        data: Generate
    });
});

const checkArtworkStatus = async (metaId) => {
    const { data } = await axios.get(`https://api.hua-der.com/api/artworks/${metaId}`, {
        headers: {
            Authorization: 'user_token ' + process.env.HUA_DER_API_TOKEN
        }
    });
    return data;
};

app.post('/api/image/generate', async (req, res) => {
    const { id } = req.auth;
    const { model, width, height, type, prompts, negativePrompts, sampling, step, cfg, seed, LoRAs } = req.body;
    const User = await prisma.User.findUnique({
        where: {
            id: id
        }
    });
    if(!User.active || !(User.credit > 0 || User.NCCredit > 0)) {
        res.send({
            status: 403,
            message: 'Forbidden, 用户未激活或魔晶不足',
            displayMessage: '用户未激活或魔晶不足'
        });
        return;
    }
    const artBoard = kamiyaToolKit.getArtBoard(width, height);
    const config = {
        LoRAs: LoRAs,
        model: model,
        width: artBoard.width,
        height: artBoard.height,
        type: type,
        prompts: prompts,
        negativePrompts: negativePrompts,
        sampling: sampling,
        step: step,
        cfg: cfg,
        seed: seed,
        batch: 1,
        artboard: artBoard.id
    };
    if(type === 'image2image') {
        const { image, ds } = req.body;
        if(!image || !ds) {
            res.send({
                status: 400,
                message: 'Bad Request, 缺少参数 image/ds',
                displayMessage: '缺少参数 image/ds'
            });
            return;
        }
        const imageBuffer = Buffer.from(image.split(',')[1], 'base64');
        const UploadTokenResponse = await axios.post('https://api.hua-der.com/api/artworks/upload-token', {}, {
            headers: {
                Authorization: 'user_token ' + process.env.HUA_DER_API_TOKEN
            }
        });
        const UploadToken = UploadTokenResponse.data.url;
        await axios.put(UploadToken, imageBuffer);
        config.referenceImageKey = UploadTokenResponse.data.fileKey;
        config.ds = ds;
    }
    axios.post('https://api.hua-der.com/api/artworks/create', config, {
        headers: {
            Authorization: 'user_token ' + process.env.HUA_DER_API_TOKEN
        }
    }).then(async R => {
        const ArtworkHashId = R.data[0].hashid;
        const ArtworkMetaId = R.data[0].metaId;
        const Generate = await prisma.Generate.create({
            data: {
                userId: id,
                hashid: ArtworkHashId,
                metaid: ArtworkMetaId,
                metadata: JSON.stringify(config),
                createdAt: Date.now(),
                updatedAt: Date.now()
            }
        });
        Generate.id = kamiyaToolKit.numberToHash(Generate.id);
        Generate.createdAt = kamiyaToolKit.formatTime(new Date(Number(Generate.createdAt)));
        Generate.updatedAt = kamiyaToolKit.formatTime(new Date(Number(Generate.updatedAt)));
        Generate.metadata = JSON.parse(Generate.metadata);
        res.send({
            status: 200,
            message: 'OK',
            data: Generate
        });
        setArtworkCheck(ArtworkHashId, ArtworkMetaId, Generate);
    }, E => {
        console.log(E.response.data);
        res.send({
            status: 500,
            message: 'PixInkValidationError: Risky/Invalid parameters/Reach the limit of quota',
            displayMessage: E.response.data.displayMessage
        });
    });
});

app.get('/api/image/config', async (req, res) => {
    const { data } = await axios.get('https://api.hua-der.com/api/artworks/config', {
        headers: {
            Authorization: 'user_token ' + process.env.HUA_DER_API_TOKEN
        }
    });
    res.send({
        status: 200,
        message: 'OK',
        data: data
    });
});

app.get('/api/image/surpriseme', async (req, res) => {
    const { data } = await axios.get('https://api.hua-der.com/api/artworks/example-prompts', {
        Authorization: 'user_token ' + process.env.HUA_DER_API_TOKEN
    });
    const { words } = data;
    let prompt = '';
    for (const word of words) {
        prompt += word + '，';
    }
    res.send({
        status: 200,
        message: 'OK',
        data: prompt
    });
});

//deprecated code(for sd-webui)

let traceCache = {};

app.get('/api/image/listBaseModel',(req,res) => {
    res.send(JSON.parse(fs.readFileSync('./data/modelConfig.json').toString()).baseModel);
});
app.get('/api/image/listLoraModel',(req,res) => {
    res.send(JSON.parse(fs.readFileSync('./data/modelConfig.json').toString()).loraModel);
});

app.get('/api/image/trace',async (req,res) => {
    if(kamiyaToolKit.useNAIFallback) {
        res.sendStatus(514);
        return;
    }
    const { id } = req.query;
    if(id && traceCache[id]) {
        request('GET',traceCache[id] + '/sdapi/v1/progress').getBody('utf8').then(JSON.parse).done(function(response) {
            res.send({
                status: 200,
                message: 'OK',
                progress: response.progress,
            });
        },(err) =>{
            console.error(err);
            res.send(kamiyaToolKit.baseResponse.internalError);
        });
    }
});

app.post('/api/image/generate-deprecated', async (req, res) => {
    const { id } = req.auth;
    let { traceId } = req.body;
    const User = await prisma.User.findUnique({
        where: {
            id: id
        }
    });
    if(!User.active || !(User.credit > 0)) {
        res.send({
            status: 403,
            message: 'Forbidden, 用户未激活或魔晶不足'
        });
        return;
    }
    const config = {
        prompt: req.body.prompt,
        negativePrompt: req.body.negativePrompt,
        steps: Number(req.body.steps),
        scale: Number(req.body.scale),
        seed: Number(req.body.seed) || 0,
        sampler: req.body.sampler,
        width: Number(req.body.width),
        height: Number(req.body.height),
        image: req.body.image,
        model: req.body.model,
        watermark: req.body.watermark
    };

    if(kamiyaToolKit.checkImageGenerateConfig(config)) {
        let cost = 100;
        if(config.steps > 50) {
            res.send({
                status: 400,
                message: 'Bad Request, 步数过大'
            });
            return;
        }
        const resolution = kamiyaToolKit.customResolution(config.width, config.height);
        config.width = resolution.width;
        config.height = resolution.height;

        if(config.image) cost = cost * 2;
        if(config.width * config.height > 768 * 512) cost = cost * 2;

        if(kamiyaToolKit.useNAIFallback) {
            traceId = uuid();
            const NAIRequest = kamiyaToolKit.createNAIRequest(config);
            if(NAIRequest) {
                request('POST', 'https://api.novelai.net/ai/generate-image', {json: NAIRequest,headers: {'Authorization': 'Bearer ' + kamiyaToolKit.NAIToken}}).getBody().done(async (R) => {
                    fs.writeFileSync('./data/cache/' + traceId + '.zip', R);
                    const zip = new AdmZip('./data/cache/' + traceId + '.zip');
                    fs.mkdirSync('./data/cache/' + traceId);
                    zip.extractAllTo('./data/cache/' + traceId, true);
                    const file = fs.readFileSync('./data/cache/' + traceId + '/image_0.png');
                    const uploaded = await uploadImage('data:image/png;base64,' + file.toString('base64'), config.watermark);
                    res.send({
                        status: 200,
                        message: 'OK',
                        cost: cost / 100,
                        image: uploaded.url,
                        backend: 'NovelAI Fallback Service'
                    });
                    if(!uploaded.success) uploaded.url = 'Failed to upload this content,contact the admin.';
                    await prisma.User.update({
                        where: {
                            id: id
                        },
                        data: {
                            credit: {
                                decrement: cost
                            },
                            images: {
                                create: {
                                    uuid: uploaded.uuid,
                                    url: uploaded.url
                                }
                            }
                        }
                    });
                    await newBillingHistory(id, cost / 100, 'imageGenerate-NovelAI');
                    await eventLogger.newEvent('imageGenerate', {
                        ip: req.headers['x-forwarded-for'],
                        auth: req.auth,
                        body: req.body,
                        uploaded: uploaded
                    });
                });
            }
            else {
                res.send({
                    status: 400,
                    message: '当前服务处于Fallback模式，无法处理i2i请求，请稍后再试'
                });
            }
            return;
        }

        const webuiNode = kamiyaToolKit.pickWebuiNode(config.model);
        const webuiRequest = kamiyaToolKit.createWebuiRequest(config, webuiNode);

        request('POST', webuiNode[0].url + '/run/predict/', {json: webuiRequest.predictBody}).done(() => {
            if(traceId) traceCache[traceId] = webuiNode[0].url;
            request('POST',webuiNode[0].url + webuiRequest.path, {json: webuiRequest.requestBody}).getBody('utf8').then(JSON.parse).then(
                async (response) => {
                    const uploaded = await uploadImage('data:image/png;base64,' + response.images[0], config.watermark);
                    res.send({
                        status: 200,
                        message: 'OK',
                        cost: cost / 100,
                        image: uploaded.url,
                        backend: webuiNode[0].name
                    });
                    if(!uploaded.success) uploaded.url = 'Failed to upload this content,contact the admin.';
                    await prisma.User.update({
                        where: {
                            id: id
                        },
                        data: {
                            credit: {
                                decrement: cost
                            },
                            images: {
                                create: {
                                    uuid: uploaded.uuid,
                                    url: uploaded.url
                                }
                            }
                        }
                    });
                    await newBillingHistory(id, cost / 100, 'imageGenerate-Kamiya');
                    await eventLogger.newEvent('imageGenerate', {
                        ip: req.headers['x-forwarded-for'],
                        auth: req.auth,
                        body: req.body,
                        uploaded: uploaded
                    });
                }, (err) => {
                    console.error(err);
                    res.send({
                        status: 500,
                        message: 'Internal Server Error, 后端API错误 '
                    });
                }
            );
        }, (err) => {
            console.error(err);
            res.send({
                status: 500,
                message: 'Internal Server Error, 后端API错误 '
            });
        });
    }
    else res.send(kamiyaToolKit.baseResponse.badRequest);
});

app.post('/api/image/qrwaifu',async (req, res) => {
    const { id } = req.auth;
    const User = await prisma.User.findUnique({
        where: {
            id: id
        }
    });
    if(!User.active || !(User.credit > 0)) {
        res.send({
            status: 403,
            message: 'Forbidden, 用户未激活或魔晶不足'
        });
        return;
    }
    const config = {
        prompt: req.body.prompt,
        type: req.body.type,
        content: req.body.content,
        steps: req.body.steps || 28,
        weight: req.body.weight || 0.7,
        guidance_start: req.body.guidance_start || 0.14,
        guidance_end: req.body.guidance_end || 0.68,
        model: 'cetusMix_cetusVersion2Fp16'
    };
    const shortly = kamiyaToolKit.getShortly()
    request('POST', shortly, {json: {
            adminPass: process.env.ADMINPASS,
            type: config.type,
            content: config.content
        }}).getBody('utf8').then(JSON.parse).then((response) => {
        const shortlyUrl = response.url;
        QRCode.toDataURL(shortlyUrl, { errorCorrectionLevel: 'H', margin: 0.5 }, async (err, url) => {
            if(err) {
                res.send(kamiyaToolKit.baseResponse.internalError);
                return;
            }
            config.qrCode = url;
            const webuiNode = kamiyaToolKit.pickWebuiNode(config.model);
            const webuiRequest = kamiyaToolKit.createWebuiRequest(config, webuiNode, true);
            request('POST', webuiNode[0].url + '/run/predict/', {json: webuiRequest.predictBody}).done(() => {
                request('POST',webuiNode[0].url + webuiRequest.path, {json: webuiRequest.requestBody}).getBody('utf8').then(JSON.parse).then(
                    async (response) => {
                        const uploaded = await uploadImage('data:image/png;base64,' + response.images[0], config.watermark);
                        res.send({
                            status: 200,
                            message: 'OK',
                            image: uploaded.url,
                            shortly: shortlyUrl
                        });
                        if(!uploaded.success) uploaded.url = 'Failed to upload this content,contact the admin.';
                        await prisma.User.update({
                            where: {
                                id: id
                            },
                            data: {
                                credit: {
                                    decrement: 100
                                },
                                images: {
                                    create: {
                                        uuid: uploaded.uuid,
                                        url: uploaded.url
                                    }
                                }
                            }
                        });
                        await newBillingHistory(id, 1, 'imageGenerate-QRWaifu');
                        await eventLogger.newEvent('imageGenerate', {
                            ip: req.headers['x-forwarded-for'],
                            auth: req.auth,
                            body: req.body,
                            uploaded: uploaded
                        });
                    }, (err) => {
                        console.error(err);
                        res.send({
                            status: 500,
                            message: 'Internal Server Error, 后端API错误 '
                        });
                    }
                );
            }, (err) => {
                console.error(err);
                res.send({
                    status: 500,
                    message: 'Internal Server Error, 后端API错误 '
                });
            });
        });
    },(err) => {
            console.log(err);
        res.send(kamiyaToolKit.baseResponse.internalError);
    });
});

module.exports = app;

