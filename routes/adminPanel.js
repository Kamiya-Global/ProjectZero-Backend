// noinspection JSUnresolvedVariable,DuplicatedCode

require('dotenv').config();

const express = require('express');
const { v4: uuid } = require('uuid');
const kamiyaToolKit = require('../modules/toolKit');
const readLastLines = require('read-last-lines');

const app = express.Router();

const Clients = {};

const chokidar = require('chokidar');
const fs = require('fs');

const logDir = './data/eventLog/';

// 监听目录
const watcher = chokidar.watch(logDir, {ignored: /(^|[\/\\])\..|summary/, persistent: true});


watcher
    .on('add', (path) => {
        //console.log(`File ${path} has been added`);
        readLastLine(path);
    })
    .on('change', (path) => {
        console.log(`File ${path} has been changed`);
        readLastLine(path);
    });

async function readLastLine(path) {
    try {
        let lastLine = await readLastLines.read(path,1);
        if(Object.keys(Clients).length > 0) {
            for (let key in Clients) {
                lastLine = JSON.parse(lastLine);
                if(lastLine.event === 'userLogin' || lastLine.event === 'userRegister') {
                    lastLine.data.body.password = '[REDACTED]';
                    lastLine.data.body.jwt = '[REDACTED]';
                }
                Clients[key].write(`data: ${JSON.stringify(lastLine)}\n\n`);
            }
        }
        //console.log(`The last line in the file ${path} is:\n${lastLine}`);
    } catch (error) {
        console.error(`Error reading the last line from ${path}: ${error.message}`);
    }
}



app.get('/api/adminPanel/streamLog',async (req,res) => {
    const { role } = req.auth;
    if(role === 'Administrator') {
        const u = uuid();
        res.set({
            'Cache-Control': 'no-cache',
            'Content-Type': 'text/event-stream',
            'Connection': 'keep-alive'
        });
        res.flushHeaders();
        Clients[u] = res;
        await readLastLine('./data/eventLog/' + kamiyaToolKit.dateFormat('YYYY-mm-dd', new Date()) + '.jsonl');
        req.on('close', () => {
            delete Clients[u];
        });
    }
    else res.send(kamiyaToolKit.baseResponse.forbidden);
});

module.exports = app;