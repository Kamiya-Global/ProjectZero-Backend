// noinspection JSUnresolvedVariable,DuplicatedCode

require('dotenv').config();

const express = require('express');
const eventLogger = require('../modules/eventLogger');
const uploadImage = require('../modules/imageUploader');

const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

const app = express.Router();

app.get('/api/userContent/get',async (req,res) => {
    const { start, take } = req.query;
    const { id } = req.auth;
    const Image = await prisma.Image.findMany({
        where: {
            authorId: id
        },
        select: {
            uuid: true,
            url: true
        },
        orderBy: {
            id: 'desc'
        },
        skip: Number(start) - 1,
        take: Number(take)
    });
    res.send({
        status: 200,
        message: 'OK',
        data: Image
    });
});

app.get('/api/userContent/delete',async (req,res) => {
    const { id } = req.auth;
    const { uuid } = req.query;
    const Image = await prisma.Image.findUnique({
        where: {
            uuid: uuid
        }
    });
    if(!Image) {
        res.send({
            status: 404,
            message: 'Not Found'
        });
        return;
    }
    if(Image.authorId === id) {
        await prisma.Image.delete({
            where: {
                uuid: uuid
            }
        });
        await uploadImage(uuid,false,true);
        await eventLogger.newEvent('contentDelete', {
            ip: req.headers['x-forwarded-for'],
            auth: req.auth,
            body: req.query,
            deleted: Image
        });
        res.send({
            status: 200,
            message: 'OK'
        });
    } else {
        res.send({
            status: 403,
            message: 'Forbidden'
        });
    }
});

module.exports = app;
