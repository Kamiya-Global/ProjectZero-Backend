// noinspection JSUnresolvedVariable,DuplicatedCode

require('dotenv').config();

const express = require('express');
const { client: redisClient } = require('../modules/redisClient');
const { v4: uuid } = require('uuid');
const kamiyaToolKit = require('../modules/toolKit');
const eventLogger = require('../modules/eventLogger');
const tokenizer = require('gpt-3-encoder');
const newBillingHistory = require('../modules/billingHistory');
const axios = require('axios');
const { CohereStream } = require('ai');

const Anthropic = require('@anthropic-ai/sdk');

const anthropic = new Anthropic({
    apiKey: process.env.ANTHROPIC_API_KEY,
});

const { Configuration, OpenAIApi } = require('openai');
const configuration = new Configuration({
    apiKey: process.env.OPENAI_API_KEY,
    basePath: process.env.OPENAI_BASE_PATH
});
const openai = new OpenAIApi(configuration);

const { PrismaClient } = require('@prisma/client');
const fs = require("fs");
const prisma = new PrismaClient();

const app = express.Router();

app.use(express.json({ limit:'100mb'}));

function sizeContent(t) {
    return tokenizer.encode(t).length;
}

function processContext(c) {
    total = 0;
    for(let i in c) {
        total += sizeContent(c[i].content);
    }
    if(total < 4095) return c;
    else {
        let i = 4;
        while(total > 4095) {
            total -= sizeContent(c[i].content);
            c.splice(i,1);
            i++;
        }
        return c;
    }
}

const ChatGPTHandler = async (req, res) => {
    const {id} = req.auth;
    let {conversationId, content, role} = req.body;
    if (content) {
        const User = await prisma.User.findUnique({
            where: {
                id: id
            }
        });
        if (!User.active || !(User.credit > 0)) {
            res.send({
                status: 403,
                message: 'Forbidden, 用户未激活或魔晶不足'
            });
            return;
        }
        let context;

        if (conversationId) context = JSON.parse(await redisClient.get(id + '.' + conversationId)) || kamiyaToolKit.OpenAI.defaultRole();
        else {
            context = kamiyaToolKit.OpenAI.defaultRole();
            if (role) {
                const roleList = JSON.parse(fs.readFileSync('./data/roleList.json').toString());
                if (roleList[role]) {
                    context.push({'role': 'user', 'content': roleList[role].user});
                    context.push({'role': 'assistant', 'content': roleList[role].assistant});
                }
            }
            conversationId = uuid();
        }

        context.push({'role': 'user', 'content': content});

        context = processContext(context);

        openai.createChatCompletion({
            model: 'gpt-3.5-turbo',
            messages: context,
            stream: true
        }, {
            responseType: 'stream'
        }).then((R) => {
            //console.log(R);
            res.set({
                'Cache-Control': 'no-cache',
                'Content-Type': 'text/event-stream',
                'Connection': 'keep-alive'
            });
            res.flushHeaders();
            let result = '', lock = false;
            R.data.on('data', async (chunk) => {
                if (chunk.toString().match(/data: \[DONE\]/)) {
                    res.write(chunk.toString().replace('\[DONE\]', JSON.stringify({
                        id: 'kamiyaInfo',
                        conversationId: conversationId,
                        message: '[DONE]',
                        fullContent: result
                    })));
                    await prisma.user.update({
                        where: {
                            id: id
                        },
                        data: {
                            credit: {
                                decrement: 100
                            }
                        }
                    });
                    await newBillingHistory(id, 1, 'chatCompletion-Legacy');
                    if (lock) return;
                    context.push({'role': 'assistant', 'content': result});
                    await eventLogger.newEvent('chatMessage', {
                        conversationId: conversationId,
                        APIResponse: result,
                        requestBody: req.body
                    });
                    await redisClient.del(id + '.' + conversationId);
                    await redisClient.set(id + '.' + conversationId, JSON.stringify(context));
                    await redisClient.expire(id + '.' + conversationId, 60 * 60 * 24);
                    return;
                }
                try {
                    const J = JSON.parse(chunk.toString().replace('data: ', ''));
                    if (J.choices[0].delta) {
                        if (J.choices[0].delta.content) result += J.choices[0].delta.content;
                        if (result.match((/\[BLOCKED\]/))) {
                            await redisClient.del(conversationId);
                            lock = true;
                            return;
                        }
                    }
                } catch (e) {
                }
                res.write(chunk.toString());
            });
            R.data.on('end', () => {
                res.end();
            });
            req.on('close', () => {
            });
        }, async (e) => {
            console.log(e);
            res.send({
                status: '500',
                conversationId: conversationId,
                message: e + '，将此ID上报以快速定位此次错误' + conversationId
            });
            await eventLogger.newEvent('chatMessage', {
                conversationId: conversationId,
                APIResponse: e + '，将此ID上报以快速定位此次错误' + conversationId,
                requestBody: req.body
            });
        });
    } else res.send(kamiyaToolKit.baseResponse.badRequest);
};

app.post('/api/openai/chatgpt/conversation', ChatGPTHandler);

app.post('/api/openai/knitgpt/conversation', ChatGPTHandler);

const processAnthropicContent = t => t.replace(/OpenAI/g, 'Anthropic').replace(/openai/g, 'Anthropic').replace(/Openai/g, 'Anthropic').replace(/OPENAI/g, 'Anthropic').replace(/openAI/g, 'Anthropic').replace(/ChatGPT/g, 'Claude');

const AnthropicHandler = async (req, res) => {
    const { id, role } = req.auth;
    const { messages, model } = req.body;
    if(kamiyaToolKit.checkRole(role, 'Developer')) {
        let prompt = '';
        for (const message of messages) {
            switch(message.role) {
                case 'user': {
                    prompt += Anthropic.HUMAN_PROMPT + ' ' + processAnthropicContent(message.content) + '\n';
                    break;
                }
                case 'assistant': {
                    prompt += Anthropic.AI_PROMPT + ' ' + message.content + '\n';
                    break;
                }
                case 'system': {
                    prompt += Anthropic.HUMAN_PROMPT + ' ' + processAnthropicContent(message.content) + '\n';
                    break;
                }
            }
        }
        prompt += Anthropic.AI_PROMPT;
        const { top_k, top_p, temperature } = req.body;
        if(!req.body.stream) {
            const result = await anthropic.completions.create({
                prompt: prompt,
                model: model,
                top_p: top_p,
                top_k: top_k,
                temperature: temperature,
                max_tokens_to_sample: 1000,
            });
            let cost = '';
            for(const message of req.body.messages) {
                cost += message.content;
            }
            const promptTokens = sizeContent(cost), completionTokens = sizeContent(result.completion), totalTokens = promptTokens + completionTokens;
            cost += result.completion;
            cost = Math.ceil(Number(sizeContent(cost) * 0.000008 * 10000));
            await prisma.user.update({
                where: {
                    id: id
                },
                data: {
                    credit: {
                        decrement: cost
                    }
                }
            });
            await newBillingHistory(id, cost / 100, 'chatCompletion-Anthropic');
            res.send({
                id: result.log_id,
                object: 'chat.completion',
                created: new Date().getTime(),
                model: result.model,
                choices: [
                    {
                        index: 0,
                        message: {
                            role: 'assistant',
                            content: result.completion
                        }
                    }
                ],
                usage: {
                    prompt_tokens: promptTokens,
                    completion_tokens: completionTokens,
                    total_tokens: totalTokens
                }
            });
            return;
        }
        res.set({
            'Cache-Control': 'no-cache',
            'Content-Type': 'text/event-stream',
            'Connection': 'keep-alive'
        });
        res.flushHeaders();
        const stream = await anthropic.completions.create({
            prompt: prompt,
            model: model,
            top_p: top_p,
            top_k: top_k,
            temperature: temperature,
            max_tokens_to_sample: 1000,
            stream: true
        });
        let result = '';
        for await (const completion of stream) {
            const OpenAIResponse = {
                id: completion.log_id,
                object: 'chat.completion.chunk',
                created: new Date().getTime(),
                model: model,
                choices: [
                    {
                        index: 0,
                        delta: {
                            content: completion.completion
                        }
                    }
                ]
            };
            if (completion.completion) result += completion.completion;
            res.write('data: ' + JSON.stringify(OpenAIResponse) + '\n\n');
        }
        let cost = '';
        for(const message of req.body.messages) {
            cost += message.content;
        }
        cost += result;
        cost = Math.ceil(Number(sizeContent(cost) * 0.000008 * 10000));
        await prisma.user.update({
            where: {
                id: id
            },
            data: {
                credit: {
                    decrement: cost
                }
            }
        });
        await newBillingHistory(id, cost / 100, 'chatCompletion-Anthropic');
        await eventLogger.newEvent('chatCompletion', {
            ip: req.headers['x-forwarded-for'],
            auth: req.auth,
            requestBody: req.body,
            APIResponse: result
        });
        res.write('data: [DONE]\n\n');
        res.end();
    }
    else res.send(kamiyaToolKit.baseResponse.forbidden);
};

const EnhancedRolePlayHandler = async (req, res) => {
    const { id, role } = req.auth;
    const { messages, stream, model } = req.body;
    if(kamiyaToolKit.checkRole(role, 'Normal')) {
        let prompt = 'The following is a conversation with an AI assistant. The assistant is helpful, creative, clever, and very friendly.\\n\\nHuman: 你好，你能帮助我做什么？\\\\nAssistant: 我是由OpenAI创造的AI，有什么可以帮到你的吗？\\n';
        for (const message of messages) {
            switch(message.role) {
                case 'user': {
                    prompt += Anthropic.HUMAN_PROMPT + ' ' + message.content + '\n';
                    break;
                }
                case 'assistant': {
                    prompt += Anthropic.AI_PROMPT + ' ' + message.content + '\n';
                    break;
                }
            }
        }
        prompt += Anthropic.AI_PROMPT;
        openai.createCompletion({
            model: 'text-davinci-003',
            prompt: prompt,
            max_tokens: 350,
            temperature: 0.9,
            top_p: 1,
            frequency_penalty: 0,
            presence_penalty: 0.6,
            stop: [
                'Human:',
                'Assistant:'
            ],
            stream: true
        },{
            responseType: 'stream'
        }).then(R => {
            let result = '';
            res.set({
                'Cache-Control': 'no-cache',
                'Content-Type': 'text/event-stream',
                'Connection': 'keep-alive'
            });
            res.flushHeaders();
            R.data.on('data', (chunk) => {
                const data = chunk.toString();
                if(!data.includes('[DONE]')) {
                    try {
                        const originResponse = JSON.parse(data.replace('data: ', ''));
                        const OpenAIResponse = {
                            id: originResponse.id,
                            object: 'chat.completion.chunk',
                            created: originResponse.created,
                            model: model,
                            choices: [
                                {
                                    index: 0,
                                    delta: {
                                        content: originResponse.choices[0].text
                                    }
                                }
                            ]
                        };
                        result += originResponse.choices[0].text;
                        res.write('data: ' + JSON.stringify(OpenAIResponse) + '\n\n');
                    }
                    catch (e) {}
               }
           });
           R.data.on('end', async () => {
               res.write('data: [DONE]\n\n');
               res.end();
               let cost = '';
               for(const message of req.body.messages) {
                   cost += message.content;
               }
               cost += result;
               cost = Math.ceil(Number(sizeContent(cost) * 0.000008 * 10000) * 4.5)
               await prisma.user.update({
                   where: {
                       id: id
                   },
                   data: {
                       credit: {
                           decrement: cost
                       }
                   }
               });
               await newBillingHistory(id, cost / 100, 'chatCompletion-EnhancedRolePlay');
               await eventLogger.newEvent('chatCompletion', {
                   ip: req.headers['x-forwarded-for'],
                   auth: req.auth,
                   requestBody: req.body,
                   APIResponse: result
               });
           });
        }, (e) => {
            console.log(e);
            res.send(kamiyaToolKit.baseResponse.internalError);
        });
    }
    else res.send(kamiyaToolKit.baseResponse.forbidden);
}

const CohereHandler = async (req, res) => {
    const { id, role } = req.auth;
    const { messages, model, top_k, top_p, temperature } = req.body;
    let prompt = 'The following is a conversation with an AI assistant. The assistant is helpful, creative, clever, and very friendly.\\n\\nHuman: 你好，你能帮助我做什么？\\\\nAssistant: 我是由Cohere创造的AI，有什么可以帮到你的吗？\\n';
    for (const message of messages) {
        switch(message.role) {
            case 'user': {
                prompt += Anthropic.HUMAN_PROMPT + ' ' + message.content + '\n';
                break;
            }
            case 'assistant': {
                prompt += Anthropic.AI_PROMPT + ' ' + message.content + '\n';
                break;
            }
        }
    }
    prompt += Anthropic.AI_PROMPT;
    const body = {
        prompt,
        model,
        max_tokens: 350,
        stop_sequence: [
            'Human:',
            'Assistant:'
        ],
        temperature,
        k: top_k,
        p: top_p,
        return_likelihoods: 'NONE',
        stream: true
    };
    axios.post('https://api.cohere.ai/v1/generate', body,{
        headers: {
            'Authorization': 'Bearer ' + process.env.COHERE_API_KEY
        },
        responseType: 'stream'
    }).then(async R => {
        res.set({
            'Cache-Control': 'no-cache',
            'Content-Type': 'text/event-stream',
            'Connection': 'keep-alive'
        });
        res.flushHeaders();
        let result = '';
        R.data.on('data', async (chunk) => {
            try {
                const data = JSON.parse(chunk.toString());
                console.log(data)
                if(!data.is_finished) {
                    const OpenAIResponse = {
                        id: uuid(),
                        object: 'chat.completion.chunk',
                        created: new Date().getTime(),
                        model: model,
                        choices: [
                            {
                                index: 0,
                                delta: {
                                    content: data.text
                                }
                            }
                        ]
                    };
                    result += data.text;
                    res.write('data: ' + JSON.stringify(OpenAIResponse) + '\n\n');
                }
                else {
                    res.write('data: [DONE]\n\n');
                    res.end();
                    let cost = '';
                    for(const message of req.body.messages) {
                        cost += message.content;
                    }
                    cost += result;
                    cost = Math.ceil(Number(sizeContent(cost) * 0.000008 * 10000) * 4.5)
                    await prisma.user.update({
                        where: {
                            id: id
                        },
                        data: {
                            credit: {
                                decrement: cost
                            }
                        }
                    });
                    await newBillingHistory(id, cost / 100, 'chatCompletion-Cohere');
                    await eventLogger.newEvent('chatCompletion', {
                        ip: req.headers['x-forwarded-for'],
                        auth: req.auth,
                        requestBody: req.body,
                        APIResponse: result
                    });
                }
            }
            catch (e) {}
        });
    }, (e) => {
        console.log(e);
        res.send(kamiyaToolKit.baseResponse.internalError);
    });
};
const CompletionHandler =  async (req, res) => {
    const { id } = req.auth;
    let { messages, stream, model } = req.body;
    if(model.startsWith('openai:')) req.body.model = model.replace('openai:', '');
    if(model.startsWith('anthropic:')) {
        req.body.model = model.replace('anthropic:', '');
        AnthropicHandler(req, res);
        return;
    }
    if(model.startsWith('cohere:')) {
        req.body.model = model.replace('cohere:', '');
        CohereHandler(req, res);
        return;
    }
    if(req.body.top_k !== undefined) delete req.body.top_k;
    if(req.body.model === 'gpt3.5-enhanced-for-role-play' || req.body.model === 'gpt-3.5-enhanced-for-role-play') {
        EnhancedRolePlayHandler(req, res);
        return;
    }
    let openaiApi = openai;
    if(model === 'majo:gpt-3.5-turbo') openaiApi = new OpenAIApi(new Configuration({
        apiKey: process.env.OPENAI_HLB_KEY,
        basePath: process.env.OPENAI_HLB_PATH
    }));
    if(messages) {
        const User = await prisma.User.findUnique({
            where: {
                id: id
            }
        });
        if(model !== 'majo:gpt-3.5-turbo') {
            if (!User.active || !(User.credit > 0)) {
                res.send({
                    status: 403,
                    message: 'Forbidden, 用户未激活或魔晶不足'
                });
                return;
            }
        }
        else {
            if (!User.active || !(User.credit > 0 || User.NCCredit > 0)) {
                res.send({
                    status: 403,
                    message: 'Forbidden, 用户未激活或魔晶不足'
                });
                return;
            }
        }
        if(!stream) {
            openaiApi.createChatCompletion(req.body).then(async (R) => {
                res.send(R.data);
                let cost = Math.ceil(Number(R.data.usage.total_tokens * 0.000008 * 10000));
                if(req.body.model.includes('gpt-4')) cost *= 20;
                if(model !== 'majo:gpt-3.5-turbo') {
                    await prisma.user.update({
                        where: {
                            id: id
                        },
                        data: {
                            credit: {
                                decrement: cost
                            }
                        }
                    });
                    await newBillingHistory(id, cost / 100, 'chatCompletion-OpenAI');
                }
                else {
                    if(User.NCCredit > 0) {
                        await prisma.user.update({
                            where: {
                                id: id
                            },
                            data: {
                                NCCredit: {
                                    decrement: cost
                                }
                            }
                        });
                        await newBillingHistory(id, cost / 100, 'chatCompletion-Majo', 'NC');
                    }
                    else {
                        await prisma.user.update({
                            where: {
                                id: id
                            },
                            data: {
                                credit: {
                                    decrement: cost
                                }
                            }
                        });
                        await newBillingHistory(id, cost / 100, 'chatCompletion-Majo');
                    }
                }
                await eventLogger.newEvent('chatCompletion', {
                    ip: req.headers['x-forwarded-for'],
                    auth: req.auth,
                    requestBody: req.body,
                    APIResponse: R.data
                });
            }, async (e) => {
                console.log(e);
                res.send(kamiyaToolKit.baseResponse.internalError);
            });
        }
        else {
            openaiApi.createChatCompletion(req.body, {
                responseType: 'stream'
            }).then((R) => {
                //console.log(R);
                res.set({
                    'Cache-Control': 'no-cache',
                    'Content-Type': 'text/event-stream',
                    'Connection': 'keep-alive'
                });
                res.flushHeaders();
                let result = '';
                R.data.on('data', async (chunk) => {
                    res.write(chunk.toString());
                    if (chunk.toString().match(/data: \[DONE\]/)) {
                        let cost = '';
                        for(const message of req.body.messages) {
                            cost += message.content;
                        }
                        cost += result;
                        cost = Math.ceil(Number(sizeContent(cost) * 0.000008 * 10000));
                        if(req.body.model.includes('gpt-4')) cost *= 20;
                        if(model !== 'majo:gpt-3.5-turbo') {
                            await prisma.user.update({
                                where: {
                                    id: id
                                },
                                data: {
                                    credit: {
                                        decrement: cost
                                    }
                                }
                            });
                            await newBillingHistory(id, cost / 100, 'chatCompletion-OpenAI');
                        }
                        else {
                            if(User.NCCredit > 0) {
                                await prisma.user.update({
                                    where: {
                                        id: id
                                    },
                                    data: {
                                        NCCredit: {
                                            decrement: cost
                                        }
                                    }
                                });
                                await newBillingHistory(id, cost / 100, 'chatCompletion-Majo', 'NC');
                            }
                            else {
                                await prisma.user.update({
                                    where: {
                                        id: id
                                    },
                                    data: {
                                        credit: {
                                            decrement: cost
                                        }
                                    }
                                });
                                await newBillingHistory(id, cost / 100, 'chatCompletion-Majo');
                            }
                        }
                        await eventLogger.newEvent('chatCompletion', {
                            ip: req.headers['x-forwarded-for'],
                            auth: req.auth,
                            requestBody: req.body,
                            APIResponse: result
                        });
                    }
                    try {
                        const J = JSON.parse(chunk.toString().replace('data: ', ''));
                        if (J.choices[0].delta) {
                            if (J.choices[0].delta.content) result += J.choices[0].delta.content;
                        }
                    } catch (e) {}
                });
                R.data.on('end', () => {
                    res.end();
                });
                req.on('close', () => {
                });
            }, async (e) => {
                console.log(e);
                res.send(kamiyaToolKit.baseResponse.internalError);
            });
        }
    }
    else res.send(kamiyaToolKit.baseResponse.badRequest);
};

app.post('/api/openai/chat/completions', CompletionHandler);

app.post('/api/openai/v1/chat/completions', CompletionHandler);

    module.exports = app;
