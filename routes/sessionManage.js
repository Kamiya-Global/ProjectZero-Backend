// noinspection JSUnresolvedVariable,DuplicatedCode

require('dotenv').config();

const fs = require('fs');
const jwt = require('jsonwebtoken');
const express = require('express');
const kamiyaToolKit = require('../modules/toolKit');
const openIDApp = require('../modules/openIDApp');

const { client } = require('../modules/redisClient');

const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

const app = express.Router();

app.use(express.json({ limit:'5mb'}));

app.get('/api/session/getDetails',async (req, res) => {
    const { id } = req.auth;
    const User = await prisma.user.findUnique({
        where: {
            id: id
        },
        select: {
            email: true,
            role: true,
            credit: true,
            NCCredit: true,
            active: true
        }
    });
    User.credit = User.credit / 100;
    User.NCCredit = User.NCCredit / 100;
    res.send({
        status: 200,
        message: 'OK',
        data: User,
        useNAIFallback: kamiyaToolKit.useNAIFallback
    });
});

app.get('/api/session/getAppInfo', async (req, res) => {
   const { id } = req.query;
    if(id) {
        const App = openIDApp.getAppByID(id);
        if(App) {
            delete App.role;
            delete App.secret;
            res.send({
                status: 200,
                message: 'OK',
                data: App
            });
        }
        else res.send(kamiyaToolKit.baseResponse.badRequest);
    }
    else res.send(kamiyaToolKit.baseResponse.badRequest);
});

function randomCode() {
    const characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    let result = "";
    for (let i = 0; i < 10; i++) {
        result += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return result.substr(0, 5) + "-" + result.substr(5);
}

app.get('/api/session/requestOpenIDToken', async (req, res) => {
    const { appId } = req.query;
    if(appId) {
        const OpenIDApp = openIDApp.getAppByID(appId);
        if(OpenIDApp) {
            const token = randomCode();
            await client.set(token, JSON.stringify({
                appId: appId,
                id: req.auth.id,
                email: req.auth.email,
                role: req.auth.role
            }));
            await client.expire(token, 60 * 60);
            res.send({
                status: 200,
                token: token
            });
        }
        else res.send(kamiyaToolKit.baseResponse.badRequest);
    }
    else res.send(kamiyaToolKit.baseResponse.badRequest);
});

app.post('/api/session/verifyOpenIDToken', async (req, res) => {
    const { token, appId, secret } = req.body;
    if(token && appId && secret) {
        const redisContent = await client.get(token);
        if(redisContent) {
            const auth = JSON.parse(redisContent);
            if(auth.appId === appId) {
                delete auth.appId;
                if(openIDApp.verifySecret(appId, secret)) {
                    res.send({
                        status: 200,
                        data: auth
                    });
                }
                else res.send(kamiyaToolKit.baseResponse.unAuthorized);
            }
            else res.send(kamiyaToolKit.baseResponse.notFound);
        }
        else res.send(kamiyaToolKit.baseResponse.notFound);
    }
    else res.send(kamiyaToolKit.baseResponse.badRequest);
});

app.get('/api/session/app/:id', async (req, res) => {
    const { id } = req.params;
    if(id) {
        const allAppInfo = JSON.parse(fs.readFileSync('./data/allAppInfo.json').toString());
        if(allAppInfo[id]) {
            res.send({
                status: 200,
                data: allAppInfo[id]
            });
        }
        else res.send(kamiyaToolKit.baseResponse.notFound);
    }
    else res.send(kamiyaToolKit.baseResponse.badRequest);
});

module.exports = app;
