// noinspection JSUnresolvedVariable,DuplicatedCode

require('dotenv').config();

const express = require('express');
const verifyCaptcha = require('../modules/captcha');
const kamiyaToolKit = require('../modules/toolKit');
const eventLogger = require('../modules/eventLogger');
const openIDApp = require('../modules/openIDApp');
const newBillingHistory = require('../modules/billingHistory');
const userCustomConfig = require('../modules/userCustomConfig');

const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

const app = express.Router();

app.use(express.json({ limit:'5mb'}));

app.post('/api/billing/checkin', async (req, res) => {
    const { id } = req.auth;
    const { token, type } = req.body;
    if(token && type) {
        if(await verifyCaptcha(token, type)) {
            const User = await prisma.user.findUnique({
                where: {
                    id: id
                }
            });
            if(User) {
                if(kamiyaToolKit.dateFormat('YYYY-mm-dd',new Date()) !== kamiyaToolKit.dateFormat('YYYY-mm-dd', new Date(Number(User.lastCheckIn)))) {
                    await eventLogger.newEvent('userCheckin',{
                        auth: req.auth
                    });
                    await prisma.user.update({
                        where: {
                            id: id
                        },
                        data: {
                            lastCheckIn: new Date().getTime(),
                            credit: {
                                increment: 4000
                            },
                            NCCredit: {
                                increment: kamiyaToolKit.isWeekend() ? 9000 : 4500
                            }
                        }
                    });
                    await newBillingHistory(id, -40, 'checkIn-Kamiya');
                    await newBillingHistory(id, kamiyaToolKit.isWeekend() ? -90 : -45, 'checkIn-Kamiya', 'NC');
                    res.send({
                        status: 200,
                        message: '获得 40 魔晶 + ' + (kamiyaToolKit.isWeekend() ? 90 : 45) + ' 公益魔晶'
                    });
                }
                else {
                    res.send({
                        status: 400,
                        message: '今天已经签到了'
                    });
                }
            }
            else res.send(kamiyaToolKit.baseResponse.unAuthorized);
        }
        else res.send(kamiyaToolKit.baseResponse.badRequest);
    }
    else res.send(kamiyaToolKit.baseResponse.badRequest);
});

app.post('/api/billing/addCredit', async (req, res) => {
   const { adminPass, credit, id, email } = req.body;
    if(adminPass === process.env.ADMINPASS) {
        if(credit) {
            if(email) {
                await prisma.user.update({
                    where: {
                        email: email
                    },
                    data: {
                        credit: {
                            increment: Number(credit) * 100
                        }
                    }
                });
                await newBillingHistory(id, -Number(credit), 'recharge-MagicShop');
                res.send(kamiyaToolKit.baseResponse.ok);
                return;
            }
            await prisma.user.update({
                where: {
                    id: id
                },
                data: {
                    credit: {
                        increment: Number(credit) * 100
                    }
                }
            });
            await newBillingHistory(id, -Number(credit), 'recharge-MagicShop');
            res.send(kamiyaToolKit.baseResponse.ok);
        }
        else res.send(kamiyaToolKit.baseResponse.badRequest);
    }
    else res.send(kamiyaToolKit.baseResponse.unAuthorized);
});

app.post('/api/billing/charge',async (req,res) => {
    const { appId, secret, credit, id, allowNC } = req.body;
    if(appId && secret) {
        if(openIDApp.verifySecret(appId, secret) && openIDApp.checkRole(appId, 'system')) {
            if(credit) {
                if(id) {
                    const User = await prisma.User.findUnique({
                        where: {
                            id: Number(id)
                        }
                    });
                    if(!User.active) {
                        res.send({
                            status: 403,
                            message: 'user not active'
                        });
                        return;
                    }
                    if(User.credit < Number(credit) * 100 && (!allowNC || User.NCCredit < Number(credit) * 100)) {
                        res.send({
                            status: 403,
                            message: 'insufficient credit'
                        });
                        return;
                    }
                    if(User.NCCredit > 0 && allowNC) {
                        await prisma.user.update({
                            where: {
                                id: Number(id)
                            },
                            data: {
                                NCCredit: {
                                    decrement: Number(credit) * 100
                                }
                            }
                        });
                        await newBillingHistory(id, Number(credit), 'charge-OpenID', 'NC');
                        res.send(kamiyaToolKit.baseResponse.ok);
                        return;
                    }
                    await prisma.user.update({
                        where: {
                            id: Number(id)
                        },
                        data: {
                            credit: {
                                decrement: Number(credit) * 100
                            }
                        }
                    });
                    await newBillingHistory(id, Number(credit), 'charge-OpenID');
                    res.send(kamiyaToolKit.baseResponse.ok);
                }
                else res.send(kamiyaToolKit.baseResponse.badRequest);
            }
            else res.send(kamiyaToolKit.baseResponse.badRequest);
        }
        else res.send(kamiyaToolKit.baseResponse.unAuthorized);
    }
    else res.send(kamiyaToolKit.baseResponse.badRequest);
});

app.post('/api/billing/auth', async (req, res) => {
    const { appId, secret, id, allowNC } = req.body;
    if(appId && secret) {
        if(openIDApp.verifySecret(appId, secret) && openIDApp.checkRole(appId, 'system')) {
            if(id) {
                const User = await prisma.User.findUnique({
                    where: {
                        id: Number(id)
                    }
                });
                if(!User.active) {
                    res.send({
                        status: 403,
                        message: 'user not active'
                    });
                    return;
                }
                if(!allowNC) {
                    if(User.credit < 0) {
                        res.send({
                            status: 403,
                            message: 'insufficient credit'
                        });
                        return;
                    }
                }
                else {
                    if(User.credit < 0 && User.NCCredit < 0) {
                        res.send({
                            status: 403,
                            message: 'insufficient credit'
                        });
                        return;
                    }
                }
                res.send(kamiyaToolKit.baseResponse.ok);
            }
            else res.send(kamiyaToolKit.baseResponse.badRequest);
        }
        else res.send(kamiyaToolKit.baseResponse.unAuthorized);
    }
    else res.send(kamiyaToolKit.baseResponse.badRequest);
});

app.post('/api/billing/detail', async (req, res) => {
    const { appId, secret, id } = req.body;
    if(appId && secret) {
        if(openIDApp.verifySecret(appId, secret) && openIDApp.checkRole(appId, 'system')) {
            if(id) {
                const User = await prisma.User.findUnique({
                    where: {
                        id: Number(id)
                    }
                });
                delete User.id;
                delete User.password;
                delete User.lastCheckIn;
                delete User.registerAt;
                User.credit = User.credit / 100;
                User.NCCredit = User.NCCredit / 100;
                res.send({
                    status: 200,
                    message: 'OK',
                    data: User
                });
            }
            else res.send(kamiyaToolKit.baseResponse.badRequest);
        }
        else res.send(kamiyaToolKit.baseResponse.unAuthorized);
    }
    else res.send(kamiyaToolKit.baseResponse.badRequest);
});

app.post('/api/billing/openidCheckin', async (req, res) => {
    const { appId, secret, id } = req.body;
    if(appId && secret) {
        if(openIDApp.verifySecret(appId, secret) && openIDApp.checkRole(appId, 'system')) {
            if(id) {
                const User = await prisma.User.findUnique({
                    where: {
                        id: Number(id)
                    }
                });
                if(User) {
                    if(kamiyaToolKit.dateFormat('YYYY-mm-dd',new Date()) !== kamiyaToolKit.dateFormat('YYYY-mm-dd', new Date(Number(User.lastCheckIn)))) {
                        await prisma.user.update({
                            where: {
                                id: Number(id)
                            },
                            data: {
                                lastCheckIn: new Date().getTime(),
                                credit: {
                                    increment: 4000
                                },
                                NCCredit: {
                                    increment: kamiyaToolKit.isWeekend() ? 9000 : 4500
                                }
                            }
                        });
                        await newBillingHistory(id, -40, 'checkIn-OpenID');
                        await newBillingHistory(id, kamiyaToolKit.isWeekend() ? -90 : -45, 'checkIn-OpenID', 'NC');
                        res.send(kamiyaToolKit.baseResponse.ok);
                    }
                    else {
                        res.send({
                            status: 400,
                            message: 'already checkin'
                        });
                    }
                }
                else res.send(kamiyaToolKit.baseResponse.unAuthorized);
            }
            else res.send(kamiyaToolKit.baseResponse.badRequest);
        }
        else res.send(kamiyaToolKit.baseResponse.unAuthorized);
    }
    else res.send(kamiyaToolKit.baseResponse.badRequest);
});

app.post('/api/billing/forceCharge',async (req,res) => {
    const { appId, secret, credit, id, allowNC } = req.body;
    if(appId && secret) {
        if(openIDApp.verifySecret(appId, secret) && openIDApp.checkRole(appId, 'system')) {
            if(credit) {
                if(id) {
                    const User = await prisma.User.findUnique({
                        where: {
                            id: Number(id)
                        }
                    });
                    if(!User.active) {
                        res.send({
                            status: 403,
                            message: 'user not active'
                        });
                        return;
                    }
                    if(User.NCCredit > 0 && allowNC) {
                        await prisma.user.update({
                            where: {
                                id: Number(id)
                            },
                            data: {
                                NCCredit: {
                                    decrement: Number(credit) * 100
                                }
                            }
                        });
                        await newBillingHistory(id, Number(credit), 'charge-OpenID', 'NC');
                        res.send(kamiyaToolKit.baseResponse.ok);
                        return;
                    }
                    await prisma.user.update({
                        where: {
                            id: Number(id)
                        },
                        data: {
                            credit: {
                                decrement: Number(credit) * 100
                            }
                        }
                    });
                    await newBillingHistory(id, Number(credit), 'charge-OpenID');
                    res.send(kamiyaToolKit.baseResponse.ok);
                }
                else res.send(kamiyaToolKit.baseResponse.badRequest);
            }
            else res.send(kamiyaToolKit.baseResponse.badRequest);
        }
        else res.send(kamiyaToolKit.baseResponse.unAuthorized);
    }
    else res.send(kamiyaToolKit.baseResponse.badRequest);
});

app.get('/api/billing/history', async (req, res) => {
    const { start, take } = req.query;
    const { id } = req.auth;
    const History = await prisma.BillingHistory.findMany({
        where: {
            userId: id
        },
        select: {
            id: true,
            amount: true,
            reason: true,
            createdAt: true,
            type: true
        },
        orderBy: {
            id: 'desc'
        },
        skip: Number(start) - 1,
        take: Number(take)
    });
    if(History.length > 0) {
        for (let i = 0; i < History.length; i++) {
            History[i].id = kamiyaToolKit.numberToHash(History[i].id);
            History[i].createdAt = kamiyaToolKit.formatTime(new Date(Number(History[i].createdAt)));
            History[i].amount = History[i].amount / 100;
        }
    }
    res.send({
        status: 200,
        message: 'OK',
        data: History
    });
});

app.get('/api/billing/sla-compensation', async (req, res) => {
    const { id } = req.auth;
    res.send({
        status: 200,
        message: 'OK',
        data: true
    });
    return;
    const customConfig = await userCustomConfig.getConfig(id);
    if(customConfig['sla-compensation-2384']){
        res.send({
            status: 200,
            message: 'OK',
            data: true
        });
    }
    else {
        res.send({
            status: 200,
            message: 'OK',
            data: false
        });
    }
});

/*
app.post('/api/billing/sla-compensation', async (req, res) => {
    const { id } = req.auth;
    const { token, type } = req.body;
    if(token && type) {
        if(await verifyCaptcha(token, type)) {
            const User = await prisma.user.findUnique({
                where: {
                    id: id
                }
            });
            if(User) {
                const customConfig = await userCustomConfig.getConfig(id);
                if(!customConfig['sla-compensation-2384']){
                    await prisma.user.update({
                        where: {
                            id: id
                        },
                        data: {
                            credit: {
                                increment: 15000
                            },
                            NCCredit: {
                                increment: 15000
                            }
                        }
                    });
                    await userCustomConfig.setConfig(id, 'sla-compensation-2384', true);
                    await newBillingHistory(id, -150, 'sla-compensation-2384');
                    await newBillingHistory(id, -150, 'sla-compensation-2384', 'NC');
                    res.send({
                        status: 200,
                        message: '获得 150 魔晶 + ' + 150 + ' 公益魔晶'
                    });
                }
                else {
                    res.send({
                        status: 400,
                        message: 'already get'
                    });
                }
            }
            else res.send(kamiyaToolKit.baseResponse.unAuthorized);
        }
        else res.send(kamiyaToolKit.baseResponse.badRequest);
    }
    else res.send(kamiyaToolKit.baseResponse.badRequest);
});
*/

module.exports = app;
