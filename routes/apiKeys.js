// noinspection JSUnresolvedVariable,DuplicatedCode

require('dotenv').config();

const express = require('express');
const eventLogger = require('../modules/eventLogger');
const verifyCaptcha = require('../modules/captcha');
const kamiyaToolKit = require('../modules/toolKit');

const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

const app = express.Router();

function truncateString(str) {
    if (str.length <= 7) {
        return str;
    } else {
        return str.substring(0, 3) + "..." + str.substring(str.length - 4);
    }
}

app.get('/api/keys/list',async (req,res) => {
    const { id } = req.auth;
    const keys = await prisma.ApiKey.findMany({
        where: {
            userId: id
        },
        select: {
            id: true,
            key: true,
            lastUse: true
        }
    });
    if(keys.length != 0) {
        for(let i = 0; i < keys.length; i++) {
            keys[i].key = truncateString(keys[i].key);
            if(Number(keys[i].lastUse) === 0) keys[i].lastUse = 'Never';
            if(keys[i].lastUse != 'Never') keys[i].lastUse = kamiyaToolKit.formatTime(new Date(Number(keys[i].lastUse)));
        }
    }
    res.send({
        status: 200,
        message: 'OK',
        data: keys
    });
});

function generateKey() {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const keyLength = 48;
    let key = 'sk-';

    for (let i = 0; i < keyLength; i++) {
        const randomIndex = Math.floor(Math.random() * characters.length);
        key += characters[randomIndex];
    }

    return key;
}

app.post('/api/keys/new',async (req,res) => {
    const { id } = req.auth;
    const { token, type } = req.body;
    if(!await verifyCaptcha(token, type)) {
        res.send({
            status: 400,
            message: 'Bad Request'
        });
        return;
    }
    const key = generateKey();
    await prisma.ApiKey.create({
        data: {
            key: key,
            userId: id
        }
    });
    res.send({
        status: 200,
        message: 'OK',
        key: key
    });
});

app.post('/api/keys/revoke',async (req,res) => {
    const { id } = req.auth;
    const { keyId } = req.body;
    const apiKey = await prisma.ApiKey.findUnique({
        where: {
            id: Number(keyId)
        }
    });
    if(!apiKey) {
        res.send({
            status: 403,
            message: 'Forbidden'
        });
        return;
    }
    if(apiKey.userId !== id) {
        res.send({
            status: 403,
            message: 'Forbidden'
        });
        return;
    }
    await prisma.ApiKey.delete({
        where: {
            id: Number(keyId)
        }
    });
    res.send(kamiyaToolKit.baseResponse.ok);
});

module.exports = app;
