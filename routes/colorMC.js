require('dotenv').config();

const express = require('express');
const fetch = require('node-fetch');
const { v4: uuid } = require('uuid');
const verifyCaptcha = require('../modules/captcha');
const userCustomConfig = require('../modules/userCustomConfig');

const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

const app = express.Router();

app.post('/api/colorMC/applyAccount', async (req, res) => {
    const { id } = req.auth;
    const { token, type } = req.body;
    if(!await verifyCaptcha(token, type)) {
        res.send({
            status: 400,
            message: 'Bad Request'
        });
        return;
    }
    const Client = await prisma.colorMC.findUnique({
        where: {
            userId: id
        }
    });
    if(Client) {
        res.send({
            status: 409,
            message: 'Conflict'
        });
        return;
    }
    const custonConfig = await userCustomConfig.getConfig(id);
    if(!custonConfig['colorMCPackage']) {
        await userCustomConfig.setConfig(id, 'colorMCPackage', 256);
        custonConfig['colorMCPackage'] = 256;
    }
    const response = await fetch(process.env.COLORMC_MW_URL + '/api/newAccount' ,{
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            backendToken: process.env.COLORMC_MW_BACKEND_TOKEN,
            kamiyaId: id,
            clientKey: uuid(),
            size: custonConfig['colorMCPackage']
        })
    });
    const data = await response.json();
    console.log(data)
    if(data.status !== 200) {
        res.send({
            status: 500,
            message: 'Internal Server Error'
        });
        return;
    }
    await prisma.colorMC.create({
        data: {
            userId: id,
            clientId: data.Player.id,
            clientKey: data.Player.clientkey,
            createdAt: Date.now()
        }
    });
    res.send({
        status: 200,
        message: 'OK',
        clientKey: data.Player.clientkey
    });
});

app.post('/api/colorMC/syncAccount', async (req, res) => {
    const { id } = req.auth;
    const { token, type } = req.body;
    if(!await verifyCaptcha(token, type)) {
        res.send({
            status: 400,
            message: 'Bad Request'
        });
        return;
    }
    const Client = await prisma.colorMC.findUnique({
        where: {
            userId: id
        }
    });
    if(!Client) {
        res.send({
            status: 404,
            message: 'Not Found'
        });
        return;
    }
    const custonConfig = await userCustomConfig.getConfig(id);
    if(!custonConfig['colorMCPackage']) {
        await userCustomConfig.setConfig(id, 'colorMCPackage', 256);
        custonConfig['colorMCPackage'] = 256;
    }
    const checkResponse = await fetch(process.env.COLORMC_MW_URL + '/api/checkState' ,{
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            backendToken: process.env.COLORMC_MW_BACKEND_TOKEN,
            clientKey: Client.clientKey
        })
    });
    const checkData = await checkResponse.json();
    if(checkData.status !== 200) {
        res.send({
            status: 500,
            message: 'Internal Server Error'
        });
        return;
    }
    if(checkData.data.size === custonConfig['colorMCPackage']) {
        res.send({
            status: 400,
            message: 'Nothing to change'
        });
        return;
    }
    const response = await fetch(process.env.COLORMC_MW_URL + '/api/editAccount' ,{
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            backendToken: process.env.COLORMC_MW_BACKEND_TOKEN,
            clientId: Client.clientId,
            size: custonConfig['colorMCPackage']
        })
    });
    const data = await response.json();
    if(data.status !== 200) {
        res.send({
            status: 500,
            message: 'Internal Server Error'
        });
        return;
    }
    res.send({
        status: 200,
        message: 'OK',
        data: data.Player
    });
});

app.get('/api/colorMC/account', async (req, res) => {
    const { id } = req.auth;
    const Client = await prisma.colorMC.findUnique({
        where: {
            userId: id
        }
    });
    const custonConfig = await userCustomConfig.getConfig(id);
    if(!custonConfig['colorMCPackage']) {
        await userCustomConfig.setConfig(id, 'colorMCPackage', 256);
        custonConfig['colorMCPackage'] = 256;
    }
    if(!Client) {
        res.send({
            status: 404,
            message: 'Not Found',
            package: custonConfig['colorMCPackage']
        });
        return;
    }
    const response = await fetch(process.env.COLORMC_MW_URL + '/api/checkState' ,{
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            backendToken: process.env.COLORMC_MW_BACKEND_TOKEN,
            clientKey: Client.clientKey
        })
    });
    const data = await response.json();
    if(data.status !== 200) {
        res.send({
            status: 500,
            message: 'Internal Server Error'
        });
        return;
    }
    res.send({
        status: 200,
        message: 'OK',
        data: data.data,
        package: custonConfig['colorMCPackage']
    });
});

function endcodeBase64(str) {
    return Buffer.from(str).toString('base64');
}

app.get('/api/colorMC/showKey', async (req, res) => {
    const { id } = req.auth;
    const Client = await prisma.colorMC.findUnique({
        where: {
            userId: id
        }
    });
    if(!Client) {
        res.send({
            status: 404,
            message: 'Not Found'
        });
        return;
    }
    res.send({
        status: 200,
        message: 'OK',
        key: endcodeBase64(JSON.stringify({
            server: process.env.COLORMC_SERVER_URL,
            serverkey: process.env.COLORMC_SERVER_KEY,
            clientkey: Client.clientKey
        }))
    });
});

app.get('/api/colorMC/subcription', async (req, res) => {
    const { id } = req.auth;
    const Subcription = await prisma.storageSubcription.findUnique({
        where: {
            userId: id
        },
        select: {
            package: true,
            expiresAt: true
        }
    });
    if(!Subcription) {
        res.send({
            status: 404,
            message: 'Not Found'
        });
        return;
    }
    Subcription.expiresAt = new Date(Number(Subcription.expiresAt)).getTime();
    res.send({
        status: 200,
        message: 'OK',
        data: Subcription
    });
});

app.post('/api/colorMC/subcription/addByBilling', async (req, res) => {
    const { adminPass, id, userPackage, duration } = req.body;
    if(adminPass !== process.env.ADMINPASS) {
        res.send({
            status: 403,
            message: 'Forbidden'
        });
        return;
    }
    await userCustomConfig.setConfig(id, 'colorMCPackage', userPackage);
    const Subcription = await prisma.storageSubcription.findUnique({
        where: {
            userId: id
        }
    });
    if(!Subcription) {
        await prisma.storageSubcription.create({
            data: {
                userId: id,
                package: userPackage,
                expiresAt: Date.now() + duration * 24 * 60 * 60 * 1000,
                createdAt: Date.now()
            }
        });
        res.send({
            status: 200,
            message: 'OK'
        });
    }
    else {
        if(Subcription.expiresAt < Date.now()) {
            await prisma.storageSubcription.update({
                where: {
                    userId: id
                },
                data: {
                    package: userPackage,
                    expiresAt: Date.now() + duration * 24 * 60 * 60 * 1000
                }
            });
            res.send({
                status: 200,
                message: 'OK'
            });
        }
        else {
            await prisma.storageSubcription.update({
                where: {
                    userId: id
                },
                data: {
                    package: userPackage,
                    expiresAt: Number(Subcription.expiresAt) + duration * 24 * 60 * 60 * 1000
                }
            });
            res.send({
                status: 200,
                message: 'OK'
            });
        }
    }
});

module.exports = app;
