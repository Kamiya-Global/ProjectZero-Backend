// noinspection JSUnresolvedVariable,DuplicatedCode

require('dotenv').config();

const express = require('express');
const kamiyaToolKit = require('../modules/toolKit');
const request = require('then-request');
const verifyCaptcha = require('../modules/captcha');
const { v4:uuid } = require('uuid');
const { client } = require('../modules/redisClient');

const { PrismaClient } = require('@prisma/client');
const axios = require("axios");
const uploadImage = require("../modules/imageUploader");

const prisma = new PrismaClient();

const app = express.Router();

app.use(express.json({ limit:'100mb'}));

app.post('/api/mirateAccount/apply', async (req, res) => {
    const { id } = req.auth;
    const { token, captchaToken, type } = req.body;
    if(!await verifyCaptcha(captchaToken, type)) {
        res.send(kamiyaToolKit.baseResponse.badRequest);
        return;
    }
    if(token) {
        let oldKamiyaResponse = await request('POST', process.env.OLD_KAMIYA_API + '/api/migrateAccount/getDetails', {
            json: {token: token, adminPass: process.env.OLD_KAMIYA_ADMINPASS }
        });
        oldKamiyaResponse = JSON.parse(oldKamiyaResponse.getBody('utf8'));
        if(oldKamiyaResponse.success) {
            await prisma.User.update({
                where: {
                    id: id
                },
                data: {
                    credit: {
                        increment: oldKamiyaResponse.credit
                    }
                }
            });
            let userContent = oldKamiyaResponse.userContent;
            if(userContent) {
                await prisma.Image.createMany({
                    data: userContent.map((item) => {
                        return {
                            uuid: item.uuid,
                            url: item.url,
                            authorId: id
                        }
                    })
                });
            }
            await prisma.Bind.create(
                {
                    data: {
                        userId: id,
                        platform: oldKamiyaResponse.bind.platform,
                        bindId: oldKamiyaResponse.bind.bindId
                    }
                }
            )
            await request('POST', process.env.OLD_KAMIYA_API + '/api/migrateAccount/finishMigrate', {
                json: {token: token, adminPass: process.env.OLD_KAMIYA_ADMINPASS }
            });
            res.send(kamiyaToolKit.baseResponse.ok);
        }
        else res.send(kamiyaToolKit.baseResponse.notFound);
    }
    else res.send(kamiyaToolKit.baseResponse.badRequest);
});

app.post('/api/mirateAccount/applyLBW', async (req, res) => {
    const { id } = req.auth;
    const { token, captchaToken, type } = req.body;
    if(!await verifyCaptcha(captchaToken, type)) {
        res.send(kamiyaToolKit.baseResponse.badRequest);
        return;
    }
    if(token) {
        let oldKamiyaResponse = await request('GET', process.env.LBW_API + '/api/card/' + token, {
            headers: { authorization: 'Bearer ' + process.env.ADMINPASS }
        });
        oldKamiyaResponse = JSON.parse(oldKamiyaResponse.getBody('utf8'));
        oldKamiyaResponse.success = oldKamiyaResponse.status === 200 && !oldKamiyaResponse.data.deleted;
        if(oldKamiyaResponse.success) {
            oldKamiyaResponse.credit = oldKamiyaResponse.data.jf;
            await prisma.User.update({
                where: {
                    id: id
                },
                data: {
                    credit: {
                        increment: oldKamiyaResponse.credit * 100
                    }
                }
            });
            await request('DELETE', process.env.LBW_API + '/api/card/' + token + '/' + id, {
                headers: { authorization: 'Bearer ' + process.env.ADMINPASS }
            });
            res.send(kamiyaToolKit.baseResponse.ok);
        }
        else res.send(kamiyaToolKit.baseResponse.notFound);
    }
});

const setTaskProgess = async (taskId, progress) => {
    const task = JSON.parse(await client.get(taskId + '.transfer'));
    task.progress = progress;
    await client.set(taskId + '.transfer', JSON.stringify(task));
}

const setTransfer = async (taskId) => {
    const task = JSON.parse(await client.get(taskId + '.transfer'));
    let { Images, session, transferPath, userId, progress } = task;
    Images = Images.slice(progress);
    for (const i in Images) {
        let buffer;
        try {
            const response = await axios.get(Images[i].url, {
                responseType: 'arraybuffer'
            });
            buffer = Buffer.from(response.data, 'binary');
        }
        catch (e) {
            if(Images[i].url === 'Failed to upload this content,contact the admin.') {
                await prisma.Image.delete({
                    where: {
                        id: Images[i].id
                    }
                });
                await setTaskProgess(taskId, parseInt(i) + 1);
            }
            continue;
        }
        const uploadR = await axios.put(process.env.DRIVE_API + '/api/v3/file/upload', {
            last_modified: new Date().getTime(),
            mime_type: 'image/png',
            name: Images[i].uuid + '_kamiyaImage.png',
            path: transferPath,
            policy_id: process.env.DRIVE_POLICY_ID,
            size: buffer.length
        }, {
            headers: {
                cookie: session
            }
        });
        if(uploadR.data.code !== 0) {
            continue;
        }
        const uploadSessionId = uploadR.data.data.sessionID;
        const uploadR2 = await axios.post(process.env.DRIVE_API + '/api/v3/file/upload/' + uploadSessionId + '/0', buffer, {
            headers: {
                cookie: session,
                'Content-Type': 'application/octet-stream',
                "Content-Length": buffer.length
            }
        });
        if(uploadR2.data.code !== 0) continue;
        console.log(uploadSessionId + ' ' + Images[i].uuid + ' upload success');
        await prisma.Image.delete({
            where: {
                id: Images[i].id
            }
        });
        await uploadImage(Images[i].uuid,false,true);
        await setTaskProgess(taskId, parseInt(i) + 1 + progress);
    }
    await setTransferCache(userId, false);
    await removeTaskCache(taskId);
}

let transferCache;

(async () => {
    const rawCache = await client.get('transferCache');
    if(rawCache) transferCache = JSON.parse(rawCache);
    else transferCache = {};
    const rawTaskCache = await client.get('taskCache');
    if(rawTaskCache) {
        const taskCache = JSON.parse(rawTaskCache);
        for (const id of taskCache) {
            console.log('continue task ' + id);
            setTransfer(id);
        }
    }
})();

const setTaskCache = async (taskId) => {
    const rawTaskCache = await client.get('taskCache');
    let taskCache;
    if(rawTaskCache) taskCache = JSON.parse(rawTaskCache);
    else taskCache = [];
    taskCache.push(taskId);
    await client.set('taskCache', JSON.stringify(taskCache));
}

const removeTaskCache = async (taskId) => {
    const rawTaskCache = await client.get('taskCache');
    let taskCache;
    if(rawTaskCache) taskCache = JSON.parse(rawTaskCache);
    else taskCache = [];
    taskCache.splice(taskCache.indexOf(taskId), 1);
    await client.set('taskCache', JSON.stringify(taskCache));
}

const setTransferCache = async (id, value) => {
    transferCache[id] = value;
    await client.set('transferCache', JSON.stringify(transferCache));
}

app.get('/api/mirateAccount/transferImageToDrive', async (req, res) => {
    const { id } = req.auth;
    if(transferCache[id]) {
        const task = JSON.parse(await client.get(transferCache[id] + '.transfer'));
        res.send({
            status: 200,
            data: true,
            progress: task.progress / task.Images.length,
            now: task.progress,
            total: task.Images.length
        });
        return;
    }
    res.send({
        status: 200,
        data: false
    });
});

app.post('/api/mirateAccount/transferImageToDrive', async (req, res) => {
    const { id } = req.auth;
    const { email, password } = req.body;
    if(!email || !password) {
        res.send(kamiyaToolKit.baseResponse.badRequest);
        return;
    }
    if(transferCache[id]) {
        res.send({
            status: 400,
            message: '转移已开始，请勿重复点击'
        });
        return;
    }
    axios.post(process.env.DRIVE_API + '/api/v3/user/session', {
        userName: email,
        Password: password
    }).then(async ({ data, headers }) => {
        if(data.code !== 0) {
            res.send({
                status: 400,
                message: (data.msg === 'This account is not activated') ? '云上神谷账户未激活，请检查收件箱' : '账号或密码错误'
            });
            return;
        }
        const u = uuid();
        const transferPath = '/kamiyaImage_' + u;
        const session = headers['set-cookie'][0].split(';')[0];
        const Images = await prisma.Image.findMany({
            where: {
                authorId: id
            }
        });
        if(Images.length === 0) {
            res.send({
                status: 400,
                message: '没有可转移的图片'
            });
            return;
        }
        await client.set(u + '.transfer', JSON.stringify({
            session,
            transferPath,
            Images,
            progress: 0,
            userId: id
        }));
        res.send({
            status: 200,
            message: '转移已开始'
        });
        await setTransferCache(id, u);
        await setTaskCache(u);
        setTransfer(u);
    }, (E) => {
        console.log(E);
        res.send({
            status: 400,
            message: '账号或密码错误'
        });
    });

});

module.exports = app;
