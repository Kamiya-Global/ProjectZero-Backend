require('dotenv').config();

const fs = require('fs');
const request = require('then-request');

const webuiList = JSON.parse(fs.readFileSync(process.env.WORK_DIR + '/data/backend.json').toString()).webui;
for(let i in webuiList) {
    request('GET',webuiList[i].url + '/sdapi/v1/sd-models').getBody('utf8').then((R) => {
        R = JSON.parse(R);
        R = {
            online: true,
            checkpoints: R
        };
        fs.writeFileSync(process.env.WORK_DIR + '/data/backendCache/' + webuiList[i].id + '.json',JSON.stringify(R,0,2));
    },() => {
        fs.writeFileSync(process.env.WORK_DIR + '/data/backendCache/' + webuiList[i].id + '.json',JSON.stringify({online: false},0,2));
    });
}