require('dotenv').config();

const express = require('express');
require('express-async-errors');
const cors = require('cors');
const expressJwt = require('express-jwt');
const rateLimit = require('express-rate-limit');

process.env.TZ = 'Asia/Shanghai';

const app = express();

app.set('trust proxy', 2);

app.use(cors({
    //origin: ['https://www.kamiya.dev', 'https://www.closeai.us', 'https://chat.kamiya.dev', 'https://chat.closeai.us', ...(process.env.NODE_ENV !== 'production' ? ['http://127.0.0.1:11683', 'http://localhost:63342', 'http://localhost:5173'] : [])],
    origin: '*',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE'
}));

app.use(rateLimit({
    windowMs: 30 * 1000,
    max: 100,
    standardHeaders: true,
    legacyHeaders: false
}));

const unlessPath = ['/api/account/login', '/api/account/register', '/api/account/requestPasswordReset', '/api/account/resetPassword', '/api/account/verifyEmail', '/api/image/listBaseModel', '/api/image/listLoraModel', '/api/checkNetwork', '/api/billing/addCredit', '/api/billing/charge', '/api/session/verifyOpenIDToken', '/api/session/getAppInfo', '/api/billing/auth', '/api/billing/forceCharge', '/api/billing/detail', '/api/billing/openidCheckin', '/api/colorMC/subcription/addByBilling'];

function checkPath(req) {
    for (const path of unlessPath) {
        if (req.path === path) return true;
        if(/\/api\/session\/app\/*/.test(req.path)) return true;
    }
    return false;
}

function checkApiKey(req) {
    const header = req.headers['authorization'];
    if(header) {
        if(header.startsWith('Bearer ')) {
            const token = header.split('Bearer ')[1];
            if(token.startsWith('sk-')) {
                const apiKey = token.split('sk-')[1];
                if(apiKey.length === 48) return true;
            }
        }
    }
    return false;
}

app.use(expressJwt.expressjwt({
    secret: process.env.JWT_SECRET,
    algorithms: ['HS256']
}).unless({
    custom: function (req) {
        if(checkPath(req)) return true;
        if(checkApiKey(req)) return true;
        return false;
    }
}));

app.use(require('./middlewares/apiKeyAuth').unless({
    custom: function (req) {
        if(checkPath(req)) return true;
        if(!checkApiKey(req)) return true;
        return false;
    }
}));

app.get('/api/checkNetwork', (req, res) => {
    res.send({
        status: 200,
        ip: req.ip,
        message: 'OK'
    });
});

app.use(require('./routes/account'));

app.use(require('./routes/imageGenerate'));

app.use(require('./routes/sessionManage'));

app.use(require('./routes/userContent'));

app.use(require('./routes/migrateAccount'));

app.use(require('./routes/billing'));

app.use(require('./routes/kamiyaOpenAI'));

app.use(require('./routes/kamiyaGPTZero'));

//app.use(require('./routes/adminPanel'));

app.use(require('./routes/apiKeys'));

app.use(require('./routes/colorMC'));

app.listen(process.env.PORT || 11681, () => {
    console.log('Server is running on port ' + (process.env.PORT || 11681) + '.');
});
