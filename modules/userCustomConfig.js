const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

const initConfig =  async (userId) => {
    const User = await prisma.user.findUnique({
        where: {
            id: userId
        },
        select: {
            customConfig: true
        }
    });
    if(!User.customConfig) {
        await prisma.user.update({
            where: {
                id: userId
            },
            data: {
                customConfig: '{}'
            }
        });
    }
}

const getConfig = async (userId) => {
    const User = await prisma.user.findUnique({
        where: {
            id: userId
        },
        select: {
            customConfig: true
        }
    });
    if(!User.customConfig) {
        await initConfig(userId)
        return {};
    }
    return JSON.parse(User.customConfig);
}

const setConfig = async (userId, field, value) => {
    const User = await prisma.user.findUnique({
        where: {
            id: userId
        },
        select: {
            customConfig: true
        }
    });
    if(!User.customConfig) {
        await initConfig(userId)
    }
    const config = JSON.parse(User.customConfig);
    config[field] = value;
    await prisma.user.update({
        where: {
            id: userId
        },
        data: {
            customConfig: JSON.stringify(config)
        }
    });
    return config;
}

module.exports = {
    initConfig,
    getConfig,
    setConfig
}
