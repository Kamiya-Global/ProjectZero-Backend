const Jimp = require('jimp');

function waterMark(base64Image) {
    return new Promise((resolve, reject) => {
        Jimp.read(Buffer.from(base64Image.split(';base64,').pop(), 'base64'))
            .then(image => {
                // Load the watermark image using Jimp
                return Jimp.read('./watermark_small.png')
                    .then(watermark => {
                        // Calculate the position of the watermark on the image
                        const xPos = image.bitmap.width - watermark.bitmap.width - 5;
                        const yPos = image.bitmap.height - watermark.bitmap.height - 5;

                        // Compose the watermark onto the image
                        image.composite(watermark, xPos, yPos);

                        // Get the resulting image as a buffer and encode it in base64
                        const result = image.getBufferAsync(Jimp.MIME_PNG)
                            .then(buffer => {
                                const base64Result = buffer.toString('base64');
                                resolve('data:image/png;base64,' + base64Result);
                                // Do something with the resulting base64 image

                            })
                            .catch(err => {
                                reject(err);
                            });
                    })
                    .catch(err => {
                        reject(err);
                    });
            })
            .catch(err => {
                reject(err);
            });
    })
}

module.exports = waterMark;
