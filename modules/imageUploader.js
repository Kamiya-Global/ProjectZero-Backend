require('dotenv').config();

const fs = require('fs');
const request = require('then-request');
const waterMark = require('./waterMark');

const uploadImage = async (image, watermark=false, del = false) => {
    const nodeL = JSON.parse(fs.readFileSync('./data/backend.json').toString()).storage;
    if(watermark) image = await waterMark(image);
    if(del) {
        await request('POST', nodeL[Math.floor(Math.random() * nodeL.length)].url + '?token=' + process.env.ADMINPASS, {json: {delete: image}});
        return true;
    }
    const R = await request('POST',nodeL[Math.floor(Math.random() * nodeL.length)].url + '?token=' + process.env.ADMINPASS,{json: {image: image}});
    const R2 = JSON.parse(R.getBody('utf8'));
    R2.success = true;
    if(R2.url === 'Failed to upload this content,contact the admin.') {
        R2.success = false;
        R2.url = image;
    }
    return {
        uuid: R2.uuid,
        url: R2.url,
        success: R2.success
    }
}

module.exports = uploadImage;
