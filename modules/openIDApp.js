const fs = require('fs');

function getAppByID(id) {
    const openIDApp = JSON.parse(fs.readFileSync('./data/openIDApp.json').toString());
    return openIDApp[id];
}

function verifySecret(id, secret) {
    const app = getAppByID(id);
    if(app) {
        return app.secret === secret;
    }
    else return false;
}

function checkRole(id, role) {
    const app = getAppByID(id);
    if(app) {
        return app.role === role;
    }
    else return false;
}

module.exports = {
    getAppByID: getAppByID,
    verifySecret: verifySecret,
    checkRole: checkRole
};
