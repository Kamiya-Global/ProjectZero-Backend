require('dotenv').config();

const formData = require('form-data');
const Mailgun = require('mailgun.js');
const mailgun = new Mailgun(formData);
const mg = mailgun.client({username: 'api', key: process.env.MAILGUN_API_KEY});

const sendEmail = (to, subject, html) => {
    const data = {
        from: 'Kamiya <no-reply@voc.ink>',
        to: to,
        subject: subject,
        html: html
    };
    mg.messages.create('voc.ink', data).then(msg => console.log(msg)).catch(err => console.log(err));
}

class emailVerification {
    constructor(url) {
        this.html = `
        <html lang="en">
            <head>
                <title>Kamiya Email Verification</title>
            </head>
            <body>
                <h1>Kamiya Email Verification</h1>
                <p>单击下方的链接以验证您的电子邮件地址。</p>
                <p>Click the link below to verify your email address.</p>
                <a href="${url}">${url}</a>
            </body>
        </html>
        `;
    }
}

class passwordReset {
    constructor(url) {
        this.html = `
        <html lang="en">
            <head>
                <title>Kamiya Password Reset</title>
            </head>
            <body>
                <h1>Kamiya Password Reset</h1>
                <p>单击下方的链接以重置您的密码。</p>
                <p>Click the link below to reset your password.</p>
                <a href="${url}">${url}</a>
            </body>
        </html>
        `;
    }
}

const getVerificationEmail = (url) => {
    return new emailVerification(url).html;
}

const getPasswordResetEmail = (url) => {
    return new passwordReset(url).html;
}

module.exports = {
    sendEmail: sendEmail,
    getVerificationEmail: getVerificationEmail,
    getPasswordResetEmail: getPasswordResetEmail
};
