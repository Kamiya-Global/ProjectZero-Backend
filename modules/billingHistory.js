const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

const newBillingHistory = async (userId, amount, reason, type = 'C') => {
    await prisma.BillingHistory.create({
        data: {
            userId,
            amount: Math.round(amount * 100),
            reason,
            createdAt: new Date().getTime(),
            type
        }
    });
};

module.exports = newBillingHistory;
