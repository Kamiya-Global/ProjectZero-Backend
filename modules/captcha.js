// noinspection JSUnresolvedVariable,DuplicatedCode

require('dotenv').config();

const request = require('then-request');
const FormData = request.FormData;

const verifyCaptcha = (token, type) => {
    return new Promise((resolve, reject) => {
        if(!token || !type) resolve(false);
        let data = new FormData();
        let url;
        if(type === 'cloudflare') {
            url = 'https://challenges.cloudflare.com/turnstile/v0/siteverify';
            data.append('secret',process.env.CLOUDFLARE_KEY || '0x0000000');
        }
        else if(type === 'google') {
            url = 'https://www.google.com/recaptcha/api/siteverify';
            data.append('secret',process.env.GOOGLE_KEY || '0x0000000');
        }
        else {
            url = 'https://hcaptcha.com/siteverify';
            data.append('secret',process.env.HCAPTCHA_KEY || '0x0000000');
        }
        data.append('response',token);
        request('POST',url,{form: data}).getBody('utf8').then(JSON.parse).done(function(R) {
            if(!R.success) resolve(false);
            else resolve(true);
        },(e) =>{
            reject(e);
        });
    });
}

module.exports = verifyCaptcha;