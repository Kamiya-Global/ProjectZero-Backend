require('dotenv').config();

const fs = require("fs");
const imageUploader = require('./imageUploader');
const kamiyaToolKit = require('./toolKit');

const newEvent = async (type, event) => {
    const nDate = new Date();
    event.time = nDate.getTime();
    const fDate = kamiyaToolKit.dateFormat('YYYY-mm-dd', nDate);

    function increaseUsage (usageType) {
        try {
            let usage = JSON.parse(fs.readFileSync('./data/eventLog/summary/' + usageType + '.json').toString());
            if(usage[fDate]) usage[fDate] += 1;
            else usage[fDate] = 1;
            fs.writeFileSync('./data/eventLog/summary/' + usageType + '.json', JSON.stringify(usage, null, 4));
        }
        catch(e) {
            fs.writeFileSync('./data/eventLog/summary/' + usageType + '.json',JSON.stringify({}));
            increaseUsage(usageType);
        }
    }

    switch(type) {
        case 'userCheckin': {
            break;
        }
        case 'imageGenerate': {
            if(event.body.image) {
                const upload = await imageUploader(event.body.image);
                event.body.image = upload.url;
            }
            increaseUsage('imageGenerate');
            break;
        }
        case 'chatMessage': {
            increaseUsage('chatMessage');
            break;
        }
        case 'chatCompletion': {
            increaseUsage('chatMessage');
            break;
        }
        case 'GPTZeroPredict': {
            increaseUsage('GPTZeroPredict');
            break;
        }
        case 'userLogin': {
            break;
        }
        case 'userRegister': {
            break;
        }
    }
    fs.appendFileSync('./data/eventLog/' + fDate + '.jsonl', JSON.stringify({
        event: type,
        data: event
    }) + '\n');
    return true;
}

module.exports = {
    newEvent: newEvent
};