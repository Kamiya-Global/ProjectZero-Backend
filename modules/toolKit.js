// noinspection JSUnresolvedVariable,DuplicatedCode
require('dotenv').config();

const fs = require("fs");

function dateFormat(fmt, date) {
    let ret;
    const opt = {
        "Y+": date.getFullYear().toString(),
        "m+": (date.getMonth() + 1).toString(),
        "d+": date.getDate().toString(),
        "H+": date.getHours().toString(),
        "M+": date.getMinutes().toString(),
        "S+": date.getSeconds().toString()
    };
    for (let k in opt) {
        ret = new RegExp("(" + k + ")").exec(fmt);
        if (ret) {
            fmt = fmt.replace(ret[1], (ret[1].length === 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")));
        }
    }
    return fmt;
}

function pickWebuiNode (name) {
    const webuiL = JSON.parse(fs.readFileSync('./data/backend.json').toString()).webui;
    let cacheL = [];
    for(let i in webuiL) {
        const cache = JSON.parse(fs.readFileSync('./data/backendCache/' + webuiL[i].id + '.json').toString());
        cache.webui = webuiL[i];
        if(cache.online) cacheL.push(cache);
    }
    let model;
    let cacheH = [];
    for(let i in cacheL) {
        for(let j in cacheL[i].checkpoints) {
            if(cacheL[i].checkpoints[j].model_name === name) {
                model = cacheL[i].checkpoints[j].title;
                cacheL[i].model = model;
                cacheH.push(cacheL[i]);
            }
        }
    }
    if(cacheH.length < 1) return pickWebuiNode('anything-v5-PrtRE');
    if(!model) return pickWebuiNode('anything-v5-PrtRE');
    const r = Math.floor(Math.random() * cacheH.length);
    return [cacheH[r].webui,cacheH[r].model];
}

const kamiyaToolKit = {
    baseResponse: {
        notFound: {
            status: 404,
            message: 'Not Found'
        },
        badRequest: {
            status: 400,
            message: 'Bad Request'
        },
        forbidden: {
            status: 403,
            message: 'Forbidden'
        },
        unAuthorized: {
            status: 401,
            message: 'Unauthorized'
        },
        internalError: {
            status: 500,
            message: 'Internal Server Error'
        },
        ok: {
            status: 200,
            message: 'OK'
        }
    },
    OpenAI: {
        defaultRole: () => {
            return [
                {
                    'role': 'system',
                    'content': '你是由OpenAI开发的人工智能助手，现在的时间是' + dateFormat("YYYY-mm-dd HH:MM:SS", new Date())
                },
                {'role': 'user', 'content': '你是谁？'},
                {'role': 'assistant', 'content': '你好，我是由OpenAI创造的人工智能。'}
            ];
        }
    },
    checkImageGenerateConfig: function (config) {
        const check1 = /^[0-9]+.?[0-9]*/;
        const sampler = ['Euler a','Euler','LMS','PLMS','DDIM','DPM++ 2M','DPM++ 2M Karras','DPM adaptive'];
        //const wh = ['landscape','portrait','square','landscape_l','portrait_l','square_l','custom','image'];
        if(check1.test(config.steps) && check1.test(config.scale) && check1.test(config.seed) && check1.test(config.width) && check1.test(config.height)) {
            if((sampler.indexOf(config.sampler) !== -1)) {
                return true;
            }
        }
        return false;
    },
    pickWebuiNode: pickWebuiNode,
    customResolution: function (width, height ,customMax) {
        const maxResolution = customMax || 1024; //Max Square Resolution
        const s = [width, height];
        if(s.length === 2) {
            //console.log(s);
            let r = {};
            if(s[0] * 1 > s[1] * 1) {
                r.width = maxResolution;
                r.height = Math.floor(maxResolution * (s[1]/s[0]));
                let ad = 0;
                while (ad < r.height) {
                    ad += 64;
                }
                r.height = ad;
            }
            else {
                r.width = Math.floor(maxResolution * (s[0]/s[1]));
                r.height = maxResolution;
                let ad = 0;
                while (ad < r.width) {
                    ad += 64;
                }
                r.width = ad;
            }
            if(s[0] < maxResolution && s[1] < maxResolution) {
                r.width = s[0];
                let ad = 0;
                while (ad < r.width) {
                    ad += 64;
                }
                r.width = ad;
                r.height = s[1];
                ad = 0;
                while (ad < r.height) {
                    ad += 64;
                }
                r.height = ad;
            }
            return r;
        }
    },
    createNAIRequest: function (config) {
        if(config.image) return false;
        return {
            action: 'generate',
            input: config.prompt,
            model: 'safe-diffusion',
            parameters: {
                height: config.height,
                width: config.width,
                n_samples: 1,
                negative_prompt: config.negativePrompt,
                qualityToggle: true,
                scale: config.scale,
                seed: config.seed,
                sm: false,
                sm_dyn: false,
                steps: config.steps,
                ucPreset: 0,
                sampler: 'k_dpmpp_2m'
            }
        }
    },
    createWebuiRequest: function (config, node, isQrWaifu = false) {
        let res = {};
        const model = node[1];
        node = node[0];
        res.predictBody = {
            data: [model],
            fn_index: node.predict_index
        };
        if(isQrWaifu) {
            res.path = '/sdapi/v1/txt2img';
            res.requestBody = {
                prompt: config.prompt,
                negative_prompt: 'disfigured, kitsch, ugly, oversaturated, grain, low-res, Deformed, blurry, bad anatomy, disfigured, poorly drawn face, mutation, mutated, extra limb, ugly, poorly drawn hands, missing limb, blurry, floating limbs, disconnected limbs, malformed hands, blur, out of focus, long neck, long body, ugly, disgusting, poorly drawn, childish, mutilated, mangled, old, surreal, calligraphy, sign, writing, watermark, text, body out of frame, extra legs, extra arms, extra feet, out of frame, poorly drawn feet, cross-eye, blurry, bad anatomy',
                prompt_style: 'None',
                prompt_style2: 'None',
                steps: Number(config.steps),
                sampler_index: "Euler a",
                restore_faces: false,
                tiling: false,
                n_iter: 1,
                batch_size: 1,
                cfg_scale: 6.5,
                seed: 0,
                subseed: -1,
                subseed_strength: 0,
                seed_resize_from_h: 0,
                seed_resize_from_w: 0,
                seed_enable_extras: false,
                height: 512,
                width: 512,
                enable_hr: false,
                denoising_strength: 0.7,
                firstphase_width: 0,
                firstphase_height:0,
                alwayson_scripts: {
                    controlnet: {
                        args: [
                            {
                                input_image: config.qrCode,
                                module: 'inpaint_global_harmonious',
                                model: 'control_v1p_sd15_brightness',
                                weight: Number(config.weight),
                                guidance_start: Number(config.guidance_start),
                                guidance_end: Number(config.guidance_end)
                            }
                        ]
                    }
                }
            };
            return res;
        }
        if(config.image) {
            res.path = '/sdapi/v1/img2img';
            res.requestBody = {
                mode: 0,
                prompt: config.prompt,
                negative_prompt: config.negativePrompt,
                prompt_style: 'None',
                prompt_style2: 'None',
                init_images: [config.image],
                mask_mode: 'Draw mask',
                steps: config.steps,
                sampler_index: config.sampler,
                mask_blur: 4,
                inpainting_fill: 0,
                restore_faces: false,
                tiling: false,
                n_iter: 1,
                batch_size: 1,
                cfg_scale: config.scale,
                seed: config.seed,
                subseed: -1,
                subseed_strength: 0,
                seed_resize_from_h: 0,
                seed_resize_from_w: 0,
                seed_enable_extras: false,
                resize_mode: 0,
                inpaint_full_res: false,
                inpaint_full_res_padding: 32,
                inpaint_mask_invert: 'Inpaint masked',
                height: config.height,
                width: config.width,
                enable_hr: false,
                denoising_strength: 0.75,
                firstphase_width: 0,
                firstphase_height:0
            };
        }
        else {
            res.path = '/sdapi/v1/txt2img';
            res.requestBody = {
                prompt: config.prompt,
                negative_prompt: config.negativePrompt,
                prompt_style: 'None',
                prompt_style2: 'None',
                steps: config.steps,
                sampler_index: config.sampler,
                restore_faces: false,
                tiling: false,
                n_iter: 1,
                batch_size: 1,
                cfg_scale: config.scale,
                seed: config.seed,
                subseed: -1,
                subseed_strength: 0,
                seed_resize_from_h: 0,
                seed_resize_from_w: 0,
                seed_enable_extras: false,
                height: config.height,
                width: config.width,
                enable_hr: false,
                denoising_strength: 0.7,
                firstphase_width: 0,
                firstphase_height:0
            };
        }
        return res;
    },
    dateFormat: dateFormat,
    useNAIFallback: JSON.parse(fs.readFileSync('./data/backend.json').toString()).config.useNAIFallback,
    NAIToken: JSON.parse(fs.readFileSync('./data/backend.json').toString()).nai.token,
    getShortly: function () {
        return JSON.parse(fs.readFileSync('./data/backend.json').toString()).shortly;
    },
    checkRole: function (userRole, minRole) {
        const role = ['Normal', 'Developer', 'Administrator'];
        if(role.indexOf(userRole) < role.indexOf(minRole)) return false;
        return true;
    },
    formatTime: function (date) {
        const currentYear = date.getFullYear();

        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate();
        let hours = date.getHours();
        let minutes = date.getMinutes();
        let seconds = date.getSeconds();

        month = (month < 10 ? '0' : '') + month;
        day = (day < 10 ? '0' : '') + day;
        hours = (hours < 10 ? '0' : '') + hours;
        minutes = (minutes < 10 ? '0' : '') + minutes;
        seconds = (seconds < 10 ? '0' : '') + seconds;

        let formattedTime = `${month}-${day} ${hours}:${minutes}:${seconds}`;

        if (year !== currentYear) {
            formattedTime = `${year}-${formattedTime}`;
        }

        return formattedTime;
    },
    numberToHash: function (num) {
        num = num * Number(process.env.HASH_SALT_SUB) + Number(process.env.HASH_SALT_MAIN);
        const chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
        const base = chars.length;
        let hash = '';
        while (num > 0) {
            hash = chars[num % base] + hash;
            num = Math.floor(num / base);
        }
        while (hash.length < 5) {
            hash = chars[0] + hash;
        }
        return hash;
    },
    hashToNumber: function (hash) {
        const chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
        const base = chars.length;
        let num = 0;
        for (let i = 0; i < hash.length; i++) {
            num = num * base + chars.indexOf(hash[i]);
        }
        return (num - Number(process.env.HASH_SALT_MAIN)) / Number(process.env.HASH_SALT_SUB);
    },
    getArtBoard: function (width, height) {
        const ArtBoard = [
            {
                "id": "480x854",
                "name": "9:16 长图（480 x 854） 消耗3点",
                "width": 480,
                "height": 854,
                "points": 3
            },
            {
                "id": "512x768",
                "name": "3:4 长图（512 x 768） 消耗3点",
                "width": 512,
                "height": 768,
                "points": 3
            },
            {
                "id": "640x640",
                "name": "1:1 方形（640 x 640） 消耗3点",
                "width": 640,
                "height": 640,
                "points": 3
            },
            {
                "id": "768x512",
                "name": "4:3 宽图（768 x 512） 消耗3点",
                "width": 768,
                "height": 512,
                "points": 3
            },
            {
                "id": "854x480",
                "name": "16:9 宽图（854 x 480） 消耗3点",
                "width": 854,
                "height": 480,
                "points": 3
            },
            {
                "id": "1024x576",
                "name": "16:9 宽图（1024 x 576） 消耗4点",
                "width": 1024,
                "height": 576,
                "points": 4
            },
            {
                "id": "1280x720",
                "name": "16:9 宽图（1280 x 720） 消耗5点",
                "width": 1280,
                "height": 720,
                "points": 5
            },
            {
                "id": "1344x576",
                "name": "21:9 超宽图（1344 x 576） 消耗5点",
                "width": 1344,
                "height": 576,
                "points": 5
            },
            {
                "id": "576x1024",
                "name": "9:16 长图（576 x 1024） 消耗4点",
                "width": 576,
                "height": 1024,
                "points": 4
            },
            {
                "id": "576x1344",
                "name": "21:9 超长图（576 x 1344） 消耗5点",
                "width": 576,
                "height": 1344,
                "points": 5
            },
            {
                "id": "720x1280",
                "name": "9:16 长图（720 x 1280） 消耗5点",
                "width": 720,
                "height": 1280,
                "points": 5
            }
        ];
        for (let i = 0; i < ArtBoard.length; i++) {
            if (ArtBoard[i].width === width && ArtBoard[i].height === height) {
                return ArtBoard[i];
            }
        }
        let min = 100000;
        let res = {};
        for (let i = 0; i < ArtBoard.length; i++) {
            const diff = Math.abs(ArtBoard[i].width / ArtBoard[i].height - width / height);
            if (diff < min) {
                min = diff;
                res = ArtBoard[i];
            }
        }
        return res;
    },
    isWeekend: function () {
        const now = new Date();
        const day = now.getDay();
        if (day === 0 || day === 6) {
            return true;
        }
        return false;
    }
}

module.exports = kamiyaToolKit;
