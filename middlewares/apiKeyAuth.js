const { unless } = require("express-unless");

const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

const auth = async (req, res, next) => {
    const header = req.headers['authorization'];
    if(header) {
        if(header.startsWith('Bearer ')) {
            const token = header.split('Bearer ')[1];
            if(token.startsWith('sk-')) {
                const apiKey = token.split('sk-')[1];
                if(apiKey.length === 48) {
                    const Key = await prisma.ApiKey.findUnique({
                        where: {
                            key: 'sk-' + apiKey
                        }
                    });
                    if(Key) {
                        await prisma.ApiKey.update({
                            where: {
                                key: 'sk-' + apiKey
                            },
                            data: {
                                lastUse: new Date().getTime()
                            }
                        });
                        const User = await prisma.User.findUnique({
                            where: {
                                id: Key.userId
                            },
                            select: {
                                id: true,
                                email: true,
                                role: true,
                            }
                        });
                        req.auth = User;
                        next();
                        return;
                    }
                }
            }
        }
    }
    res.status(401).send({
        status: 401,
        message: 'Unauthorized'
    });
}

auth.unless = unless;

module.exports = auth;
