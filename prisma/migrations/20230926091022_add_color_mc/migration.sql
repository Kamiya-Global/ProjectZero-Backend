-- CreateTable
CREATE TABLE `ColorMC` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `clientId` INTEGER NOT NULL,
    `clientKey` VARCHAR(255) NOT NULL,
    `status` VARCHAR(255) NOT NULL DEFAULT 'created',
    `createdAt` BIGINT NOT NULL,

    UNIQUE INDEX `clientId`(`clientId`),
    UNIQUE INDEX `clientKey`(`clientKey`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
