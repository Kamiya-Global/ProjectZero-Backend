/*
  Warnings:

  - Added the required column `userId` to the `ColorMC` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `ColorMC` ADD COLUMN `userId` INTEGER NOT NULL;

-- AddForeignKey
ALTER TABLE `ColorMC` ADD CONSTRAINT `ColorMC_userId_fkey` FOREIGN KEY (`userId`) REFERENCES `User`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
