/*
  Warnings:

  - You are about to drop the column `lastCheckin` on the `User` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE `User` DROP COLUMN `lastCheckin`,
    ADD COLUMN `lastCheckIn` BIGINT NOT NULL DEFAULT 0,
    ADD COLUMN `registerAt` BIGINT NOT NULL DEFAULT 0;
