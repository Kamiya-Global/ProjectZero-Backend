-- CreateTable
CREATE TABLE `Generate` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `hashid` VARCHAR(255) NOT NULL,
    `metaid` VARCHAR(255) NOT NULL,
    `status` VARCHAR(255) NOT NULL DEFAULT 'created',
    `metadata` TEXT NOT NULL,
    `userId` INTEGER NOT NULL,
    `createdAt` BIGINT NOT NULL,
    `updatedAt` BIGINT NOT NULL,

    UNIQUE INDEX `hashid`(`hashid`),
    UNIQUE INDEX `metaid`(`metaid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `Generate` ADD CONSTRAINT `Generate_userId_fkey` FOREIGN KEY (`userId`) REFERENCES `User`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
