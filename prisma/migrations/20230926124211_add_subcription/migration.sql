-- CreateTable
CREATE TABLE `StorageSubcription` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `package` INTEGER NOT NULL,
    `expiresAt` BIGINT NOT NULL,
    `createdAt` BIGINT NOT NULL,
    `userId` INTEGER NOT NULL,

    UNIQUE INDEX `userId`(`userId`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `StorageSubcription` ADD CONSTRAINT `StorageSubcription_userId_fkey` FOREIGN KEY (`userId`) REFERENCES `User`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
